package com.alexproj.tradingsystem.repo;

import com.alexproj.tradingsystem.model.Trade;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface TradeRepository extends JpaRepository<Trade, UUID> {
    List<Trade> findAllByFigi(String figi);
}
