package com.alexproj.tradingsystem.repo;

import com.alexproj.tradingsystem.dto.CandleDto;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class PythonIndicatorRepository {

    private static final String PYTHON_HOST_RSI = "http://localhost:5000/rsi";
    private static final String PYTHON_HOST_BOLLINGER_LOW = "http://localhost:5000/bollinger/low";

    public BigDecimal rsi(List<CandleDto> candles) {
        return sendRequestToCalculateIndicator(candles, PYTHON_HOST_RSI);
    }

    public BigDecimal bollingerLow(List<CandleDto> candles) {
        return sendRequestToCalculateIndicator(candles, PYTHON_HOST_BOLLINGER_LOW);
    }

    @NotNull
    private BigDecimal sendRequestToCalculateIndicator(List<CandleDto> candles, String pythonHostBollingerLow) {
        List<Double> closes = candles.stream().map(CandleDto::getClose).map(BigDecimal::doubleValue).collect(Collectors.toList());
        RestTemplate req = new RestTemplate();

        String responseEntity = req.postForObject(
                pythonHostBollingerLow,
                closes,
                String.class);


        if (responseEntity == null) {
            throw new RuntimeException("No response for rsi");
        }
        return new BigDecimal(responseEntity);
    }
}
