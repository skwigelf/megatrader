package com.alexproj.tradingsystem.repo;

import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.mapper.CandleMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.ta4j.core.Bar;
import org.ta4j.core.BarSeries;
import org.ta4j.core.BaseBarSeries;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.bollinger.BollingerBandsLowerIndicator;
import org.ta4j.core.indicators.bollinger.BollingerBandsMiddleIndicator;
import org.ta4j.core.indicators.bollinger.BollingerBandsUpperIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.statistics.StandardDeviationIndicator;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.IntStream;

@Component
@AllArgsConstructor
public class Ta4jIndicatorRepository {

    public BigDecimal sma(List<CandleDto> candles, int period) {
        if (candles.isEmpty()) {
            return BigDecimal.ZERO;
        }
        List<Bar> bars = candles.stream().map(CandleMapper::mapToBar).toList();

        BarSeries barSeries = new BaseBarSeries(bars);


        ClosePriceIndicator closePrice = new ClosePriceIndicator(barSeries);

        return BigDecimal.valueOf(new SMAIndicator(closePrice, period).getValue(bars.size() - 1).doubleValue());
    }

    public BigDecimal rsi(List<CandleDto> candles, int period) {
        if (candles.isEmpty()) {
            return BigDecimal.ZERO;
        }
        List<Bar> bars = candles.stream().map(CandleMapper::mapToBar).toList();

        BarSeries barSeries = new BaseBarSeries(bars);

        ClosePriceIndicator closePrice = new ClosePriceIndicator(barSeries);

        return BigDecimal.valueOf(new RSIIndicator(closePrice, period).getValue(bars.size() - 1).doubleValue());
    }

    public BigDecimal bollingerUp(List<CandleDto> candles, int period) {
        if (candles.isEmpty()) {
            return BigDecimal.ZERO;
        }
        List<Bar> bars = candles.stream().map(CandleMapper::mapToBar).toList();
        BarSeries barSeries = new BaseBarSeries(bars);
        ClosePriceIndicator closePrice = new ClosePriceIndicator(barSeries);
        SMAIndicator smaIndicator14 = new SMAIndicator(closePrice, period);
        BollingerBandsMiddleIndicator middle = new BollingerBandsMiddleIndicator(smaIndicator14);
        StandardDeviationIndicator standardDeviationIndicator = new StandardDeviationIndicator(closePrice, period);
        BollingerBandsUpperIndicator bollingerBandsUpperIndicator = new BollingerBandsUpperIndicator(middle, standardDeviationIndicator);

        return BigDecimal.valueOf(bollingerBandsUpperIndicator.getValue(bars.size() - 1).doubleValue());
    }

    public BigDecimal bollingerLow(List<CandleDto> candles, int period) {
        if (candles.isEmpty()) {
            return BigDecimal.ZERO;
        }
        List<Bar> bars = candles.stream().map(CandleMapper::mapToBar).toList();
        BarSeries barSeries = new BaseBarSeries(bars);
        ClosePriceIndicator closePrice = new ClosePriceIndicator(barSeries);
        SMAIndicator smaIndicator14 = new SMAIndicator(closePrice, period);
        BollingerBandsMiddleIndicator middle = new BollingerBandsMiddleIndicator(smaIndicator14);
        StandardDeviationIndicator standardDeviationIndicator = new StandardDeviationIndicator(closePrice, period);
        BollingerBandsLowerIndicator bollingerBandsLowerIndicator = new BollingerBandsLowerIndicator(middle, standardDeviationIndicator);

        return BigDecimal.valueOf(bollingerBandsLowerIndicator.getValue(bars.size() - 1).doubleValue());
    }

    public BigDecimal bollingerMiddle(List<CandleDto> candles, int period) {
        if (candles.isEmpty()) {
            return BigDecimal.ZERO;
        }
        List<Bar> bars = candles.stream().map(CandleMapper::mapToBar).toList();
        BarSeries barSeries = new BaseBarSeries(bars);
        ClosePriceIndicator closePrice = new ClosePriceIndicator(barSeries);
        SMAIndicator smaIndicator14 = new SMAIndicator(closePrice, period);
        BollingerBandsMiddleIndicator middle = new BollingerBandsMiddleIndicator(smaIndicator14);
        StandardDeviationIndicator standardDeviationIndicator = new StandardDeviationIndicator(closePrice, period);
        BollingerBandsLowerIndicator bollingerBandsLowerIndicator = new BollingerBandsLowerIndicator(middle, standardDeviationIndicator);

        return BigDecimal.valueOf(bollingerBandsLowerIndicator.getValue(bars.size() - 1).doubleValue());
    }

    public BigDecimal ema(List<CandleDto> candles, int period) {
        List<Bar> bars = candles.stream().map(CandleMapper::mapToBar).toList();

        BarSeries barSeries = new BaseBarSeries(bars);

        ClosePriceIndicator closePrice = new ClosePriceIndicator(barSeries);

        return BigDecimal.valueOf(new EMAIndicator(closePrice, period).getValue(bars.size() - 1).doubleValue());
    }

    public List<BigDecimal> emas(List<CandleDto> candles, int period) {
        List<Bar> bars = candles.stream().map(CandleMapper::mapToBar).toList();

        BarSeries barSeries = new BaseBarSeries(bars);

        ClosePriceIndicator closePrice = new ClosePriceIndicator(barSeries);

        EMAIndicator emaIndicator = new EMAIndicator(closePrice, period);

        return IntStream.range(0, bars.size() - 1).mapToObj(i -> BigDecimal.valueOf(emaIndicator.getValue(i).doubleValue())).toList();
        //return BigDecimal.valueOf(new EMAIndicator(closePrice, period).getValue(bars.size() - 1).doubleValue());
    }
}
