package com.alexproj.tradingsystem.mapper;

import com.alexproj.tradingsystem.dto.CandleDto;
import org.ta4j.core.Bar;
import org.ta4j.core.BaseBar;
import org.ta4j.core.num.PrecisionNum;
import ru.tinkoff.invest.openapi.model.rest.Candle;
import ru.tinkoff.invest.openapi.model.rest.CandleResolution;

import java.time.Duration;

import static java.time.temporal.ChronoUnit.MONTHS;

public class CandleMapper {

    public static CandleDto map(Candle candle) {
        return CandleDto.builder()
                .close(candle.getC())
                .open(candle.getO())
                .high(candle.getH())
                .low(candle.getL())
                .volume(candle.getV().longValue())
                .figi(candle.getFigi())
                .time(candle.getTime())
                .interval(getCandleResolutionFromDuration(candle.getInterval()))
                .build();
    }

    public static Candle map(CandleDto candleDto) {
        Candle candle = new Candle();

        candle.setC(candleDto.getClose());
        candle.setV(candleDto.getVolume().intValue());
        candle.setFigi(candleDto.getFigi());
        candle.setH(candleDto.getHigh());
        candle.setL(candleDto.getLow());
        candle.setO(candleDto.getOpen());
        candle.setTime(candleDto.getTime());
        candle.setInterval(getCandleResolutionFromDuration(candleDto.getInterval()));
        return candle;
    }

    public static Duration getCandleResolutionFromDuration(CandleResolution interval) {
        if (interval == CandleResolution._1MIN) {
            return Duration.ofMinutes(1);
        }
        if (interval == CandleResolution._2MIN) {
            return Duration.ofMinutes(2);
        }
        if (interval == CandleResolution._5MIN) {
            return Duration.ofMinutes(5);
        }
        if (interval == CandleResolution._10MIN) {
            return Duration.ofMinutes(10);
        }
        if (interval == CandleResolution._15MIN) {
            return Duration.ofMinutes(15);
        }
        if (interval == CandleResolution._30MIN) {
            return Duration.ofMinutes(30);
        }
        if (interval == CandleResolution.HOUR) {
            return Duration.ofMinutes(60);
        }
        if (interval == CandleResolution.DAY) {
            return Duration.ofDays(1);
        }
        if (interval == CandleResolution.WEEK) {
            return Duration.ofDays(7);
        }
        if (interval == CandleResolution.MONTH) {
            return Duration.ofDays(30);
        }
        return null;
    }

    public static Bar mapToBar(CandleDto candle) {

        return BaseBar.builder()
                .lowPrice(PrecisionNum.valueOf(candle.getLow(), 4))
                .highPrice(PrecisionNum.valueOf(candle.getHigh(), 4))
                .openPrice(PrecisionNum.valueOf(candle.getOpen(), 4))
                .closePrice(PrecisionNum.valueOf(candle.getClose(), 4))
                .volume(PrecisionNum.valueOf(candle.getVolume()))
                .timePeriod(candle.getInterval())
                .endTime(candle.getTime().toZonedDateTime())
                .amount(PrecisionNum.valueOf(10))
                .trades(10)
                .build();

    }

    public static CandleResolution getCandleResolutionFromDuration(Duration duration) {
        if (duration.equals(Duration.ofMinutes(1))) {
            return CandleResolution._1MIN;
        }
        if (duration.equals(Duration.ofMinutes(2))) {
            return CandleResolution._2MIN;
        }
        if (duration.equals(Duration.ofMinutes(5))) {
            return CandleResolution._5MIN;
        }
        if (duration.equals(Duration.ofMinutes(10))) {
            return CandleResolution._10MIN;
        }
        if (duration.equals(Duration.ofMinutes(15))) {
            return CandleResolution._15MIN;
        }
        if (duration.equals(Duration.ofMinutes(30))) {
            return CandleResolution._30MIN;
        }
        if (duration.equals(Duration.ofHours(1))) {
            return CandleResolution.HOUR;
        }
        if (duration.equals(Duration.ofDays(1))) {
            return CandleResolution.DAY;
        }
        if (duration.equals(Duration.ofDays(7))) {
            return CandleResolution.WEEK;
        }
        if (duration.equals(Duration.of(1, MONTHS))) {
            return CandleResolution.MONTH;
        }
        return CandleResolution.DAY;
    }
}
