package com.alexproj.tradingsystem.mapper;

import com.alexproj.tradingsystem.dto.OrderDto;
import com.alexproj.tradingsystem.dto.streaming.StreamingSubscriptionAction;
import com.alexproj.tradingsystem.service.dto.OrderBookDto;
import com.alexproj.tradingsystem.service.dto.OrderBookOrderDto;
import com.alexproj.tradingsystem.service.dto.TradeDirection;
import com.alexproj.tradingsystem.service.dto.TradeDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.tinkoff.piapi.contract.v1.Order;
import ru.tinkoff.piapi.contract.v1.OrderBook;
import ru.tinkoff.piapi.contract.v1.SubscriptionAction;
import ru.tinkoff.piapi.contract.v1.Trade;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.stream.Collectors;

import static com.alexproj.tradingsystem.util.MoneyUtils.createFromQuotation;

@Component
@RequiredArgsConstructor
public class TinkoffStreamingMapper {

    public SubscriptionAction map(StreamingSubscriptionAction streamingSubscriptionAction) {
        if (streamingSubscriptionAction == StreamingSubscriptionAction.SUBSCRIBE) {
            return SubscriptionAction.SUBSCRIPTION_ACTION_SUBSCRIBE;
        }
        if (streamingSubscriptionAction == StreamingSubscriptionAction.UNSUBSCRIBE) {
            return SubscriptionAction.SUBSCRIPTION_ACTION_UNSUBSCRIBE;
        }
        return SubscriptionAction.SUBSCRIPTION_ACTION_UNSPECIFIED;
    }

    public TradeDto map(Trade trade) {
        Instant instant = Instant.ofEpochSecond(trade.getTime().getSeconds(), trade.getTime().getNanos());
        return new TradeDto(trade.getQuantity(),
                map(trade.getDirection()),
                createFromQuotation(trade.getPrice()),
                OffsetDateTime.ofInstant(instant, ZoneOffset.UTC));
    }

    public OrderBookDto map(OrderBook orderBook) {
        return new OrderBookDto(
                orderBook.getDepth(),
                orderBook.getIsConsistent(),
                orderBook.getBidsList().stream().map(this::map).collect(Collectors.toList()),
                orderBook.getAsksList().stream().map(this::map).collect(Collectors.toList()),
                null,
                null,null
        );
    }

    public OrderBookOrderDto map(Order order) {
        return new OrderBookOrderDto(
                createFromQuotation(order.getPrice()),
                order.getQuantity()
        );
    }

    public TradeDirection map(ru.tinkoff.piapi.contract.v1.TradeDirection tradeDirection) {
        if (tradeDirection.name().equals(TradeDirection.TRADE_DIRECTION_BUY.name())) {
            return TradeDirection.TRADE_DIRECTION_BUY;
        }
        if (tradeDirection.name().equals(TradeDirection.TRADE_DIRECTION_SELL.name())) {
            return TradeDirection.TRADE_DIRECTION_SELL;
        }
        return TradeDirection.TRADE_DIRECTION_UNSPECIFIED;
    }
}
