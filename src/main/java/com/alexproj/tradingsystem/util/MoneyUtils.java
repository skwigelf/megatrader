package com.alexproj.tradingsystem.util;

import lombok.experimental.UtilityClass;
import ru.tinkoff.piapi.contract.v1.Quotation;

import java.math.BigDecimal;
import java.math.RoundingMode;

@UtilityClass
public class MoneyUtils {
    public static BigDecimal createFromQuotation(Quotation quotation) {
        long units = quotation.getUnits();
        int nanos = quotation.getNano();

        return BigDecimal.valueOf(units).add(BigDecimal.valueOf(nanos).divide(BigDecimal.valueOf(1000000000), 10, RoundingMode.HALF_UP));
    }

    public static Quotation createFromBigDecimal(BigDecimal value) {
        return Quotation.newBuilder()
                .setUnits(value != null ? value.longValue() : 0)
                .setNano(value != null ? value.remainder(BigDecimal.ONE).multiply(BigDecimal.valueOf(1_000_000_000)).intValue() : 0)
                .build();
    }

    public static BigDecimal createFromUnitsAndNanos(long units, int nanos) {
        return BigDecimal.valueOf(units).add(BigDecimal.valueOf(nanos).divide(BigDecimal.valueOf(1000000000), 10, RoundingMode.HALF_UP));
    }
}
