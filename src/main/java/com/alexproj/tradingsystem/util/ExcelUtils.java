package com.alexproj.tradingsystem.util;

import lombok.experimental.UtilityClass;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@UtilityClass
public class ExcelUtils {

    public static  Cell createCell(Sheet sheet, int row, int column, String value) {
        Row rowObj = sheet.getRow(row);
        if (rowObj == null) {
            rowObj = sheet.createRow(row);
        }

        Cell cell = rowObj.getCell(column);
        if (cell == null) {
            cell = rowObj.createCell(column);
        }
        cell.setCellValue(value);
        return cell;
    }

    public static  Cell createCell(Sheet sheet, int row, int column, OffsetDateTime value) {
        Row rowObj = sheet.getRow(row);
        if (rowObj == null) {
            rowObj = sheet.createRow(row);
        }

        Cell cell = rowObj.getCell(column);
        if (cell == null) {
            cell = rowObj.createCell(column);
        }
        cell.setCellValue(value.toString());
        return cell;
    }

    public static  Cell createCell(Sheet sheet, int row, int column, BigDecimal value) {
        Row rowObj = sheet.getRow(row);
        if (rowObj == null) {
            rowObj = sheet.createRow(row);
        }

        Cell cell = rowObj.getCell(column);
        if (cell == null) {
            cell = rowObj.createCell(column);
        }
        cell.setCellValue(value.doubleValue());
        return cell;
    }

    public static Cell createCell(Sheet sheet, int row, int column, int value) {
        Row rowObj = sheet.getRow(row);
        if (rowObj == null) {
            rowObj = sheet.createRow(row);
        }

        Cell cell = rowObj.getCell(column);
        if (cell == null) {
            cell = rowObj.createCell(column);
        }
        cell.setCellValue(value);
        return cell;
    }

    public static Cell createCellFormula(Sheet sheet, int row, int column, String formula) {
        Row rowObj = sheet.getRow(row);
        if (rowObj == null) {
            rowObj = sheet.createRow(row);
        }
        Cell cell = rowObj.getCell(column);
        if (cell == null) {
            cell = rowObj.createCell(column);
        }
        cell.setCellFormula(formula);
        return cell;
    }
}
