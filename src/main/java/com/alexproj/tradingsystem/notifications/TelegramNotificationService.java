package com.alexproj.tradingsystem.notifications;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TelegramNotificationService implements ServiceNotification {

    @Value("${bot.telegramToken}")
    private String telegramToken;

    @Value("${bot.telegramChatId}")
    private String telegramChatId;

    @Value("${bot.telegramToken2}")
    private String telegramToken2;

    @Value("${bot.telegramChatId2}")
    private String telegramChatId2;

    @Value("${bot.brokerType}")
    private String broker;

    @Value("${bot.mock:false}")
    private boolean mock;

    public void sendNotificationTech(String text) {
        if (mock) {
            return;
        }
        String urlString = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";
        String apiToken = telegramToken;
        String chatId = telegramChatId;
        urlString = String.format(urlString, apiToken, chatId, text);

        RestTemplate restTemplate = new RestTemplate();

        restTemplate.getForEntity(urlString, String.class);
    }

    public void sendNotification(String text) {
        if (mock) {
            return;
        }
        String urlString = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";
        String apiToken = telegramToken2;
        String chatId = telegramChatId2;
        urlString = String.format(urlString, apiToken, chatId, text);

        RestTemplate restTemplate = new RestTemplate();

        restTemplate.getForEntity(urlString, String.class);
    }
}
