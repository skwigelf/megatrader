package com.alexproj.tradingsystem.notifications;

import com.alexproj.tradingsystem.service.dto.FullDealInfo;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.math.RoundingMode;

@UtilityClass
public class NotificationMessages {

    public static String SUCCESS_TRADE_MESSAGE(FullDealInfo fullDealInfo) {
        BigDecimal buyPriceTotal = fullDealInfo.getBuyPrice().multiply(BigDecimal.valueOf(fullDealInfo.getLotBalance()));
        BigDecimal sellPriceTotal = fullDealInfo.getSellPrice().multiply(BigDecimal.valueOf(fullDealInfo.getLotBalance()));

        BigDecimal balanceMinusCommission = sellPriceTotal.add(buyPriceTotal).add(fullDealInfo.getCommission());
        return balanceMinusCommission.compareTo(BigDecimal.ZERO) >= 0
                ? "Успешная" : "Неудачная" +
                " сделка по " + fullDealInfo.getName() + " \n"
                + "Общая цена покупки: " + buyPriceTotal + "\n"
                + "Общая цена продажи: " + sellPriceTotal + "\n"
                + "Цена за шт (покупка): " + fullDealInfo.getBuyPrice() + "\n"
                + "Цена за шт (продажа): " + fullDealInfo.getBuyPrice() + "\n"
                + "Количество лотов: " + fullDealInfo.getLotBalance() + " \n"
                + "Профит (Комиссия включена): " + balanceMinusCommission + " \n"
                + "Профит в процентах (относительно депозита 100_000) " + balanceMinusCommission.divide(buyPriceTotal, 2, RoundingMode.HALF_UP);
    }

}
