package com.alexproj.tradingsystem.notifications;

public interface ServiceNotification {

    void sendNotificationTech(String text);

    void sendNotification(String text);
}
