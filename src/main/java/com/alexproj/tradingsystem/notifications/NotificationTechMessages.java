package com.alexproj.tradingsystem.notifications;

import com.alexproj.tradingsystem.service.dto.IndicatorDto;
import com.alexproj.tradingsystem.service.TradingStatus;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;

@UtilityClass
public class NotificationTechMessages {

    public static String buyDecisionMessage(String name, IndicatorDto indicatorDto) {
        String figi = indicatorDto.getMarketInstrument().getFigi();
        BigDecimal rsi = indicatorDto.getRsi();
        BigDecimal bollingerDown = indicatorDto.getBollingerDown();
        BigDecimal price = indicatorDto.getPrice();

        return "Принято решение о покупке " + name + " figi: " + figi + " :\n" +
                "RSI " + rsi + "\n" +
                "Bollinger down " + bollingerDown + "\n" +
                "Цена " + price + "\n";
    }

    public static String buyDecisionMessage(String name, BigDecimal price) {

        return "Принято решение о покупке " + name + "\n" +
                "Цена " + price + "\n";
    }

    public static String errorWhileTrading(String figi, TradingStatus status, Exception e) {
        return "Возникла ошибка во время торговли. Figi: " + figi + ", Статус " + "\n" + status + "\n" + "!" + ", Ошибка: " + e.getMessage();
    }
}
