package com.alexproj.tradingsystem.interceptors;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String authHeader = request.getHeader("Authorization");
        if (authHeader == null) {
            //return true;
            response.setStatus(401);
            return false;//throw new SecurityException();
        }
        return true;//"Bearer 1340b240-f75b-4e59-888f-fa66ce3b35b9".equals(authHeader);
    }
}