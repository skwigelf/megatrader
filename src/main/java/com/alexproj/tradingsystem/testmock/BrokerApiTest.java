package com.alexproj.tradingsystem.testmock;

import com.alexproj.tradingsystem.dto.*;
import com.alexproj.tradingsystem.service.BrokerApi;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
@ConditionalOnProperty(prefix = "bot", name = "mock", havingValue = "true")
public class BrokerApiTest implements BrokerApi {

    private BigDecimal balance = BigDecimal.valueOf(100_000);

    private BoughtStock boughtStock = null;

    private final String brokerAccountId = "111222333";

    public static final BigDecimal COMMISSION = BigDecimal.valueOf(0.0004);

    private final Map<String, String> figiNames = Map.of("BBG008F2T3T2", "Русал");

    private final List<BoughtStock> bought = new ArrayList<>();
    private final List<BoughtStock> sold = new ArrayList<>();

    private UUID currentBoughtId = UUID.randomUUID();
    private UUID currentSoldId = currentBoughtId;

    private OffsetDateTime currentTime;

    @Override
    public BigDecimal getCurrencyBalance(String brokerAccountId, CurrencyType currencyType) {
        return balance;
    }

    @Override
    public List<String> getFigisNames(List<String> figis) {
        return figis.stream().map(figiNames::get).collect(Collectors.toList());
    }

    @Override
    public PortfolioPositionDto getPortfolioPosition(String figi, SearchMarketInstrumentDto instrument, String brokerAccountId) {
        if (boughtStock != null) {
            return PortfolioPositionDto.builder()
                    .lots(boughtStock.getLots())
                    .figi(figi)
                    .time(boughtStock.getTime())
                    .build();
        }
        return null;
    }

    @Override
    public List<OrderDto> getActiveOrders(String brokerAccountId, String figi) {
        if (boughtStock != null) {
            return List.of(OrderDto.builder()
                    .figi(figi)
                    .build());
        }
        return Collections.emptyList();
    }

    @Override
    public OrderDto getActiveOrder(String brokerAccountId, String figi, String orderId) {

        return null;
    }

    @Override
    public SearchMarketInstrumentDto searchByFigi(String figi) {
        return SearchMarketInstrumentDto.builder()
                .name(figiNames.get(figi))
                .build();
    }

    @Override
    public OperationDto getLastPositionLastDay(String figi, String brokerAccountId, OperationTypeWithCommission type) {
        return null;
    }

    @Override
    public void cancelOrder(String brokerAccountId, String figi, String id) {

    }

    @Override
    public PlacedLimitOrderDto placeLimitOrder(String brokerAccountId, String figi, LimitOrderRequestDto limitOrderRequest) {
        if (limitOrderRequest.getOperation() == OperationType.BUY) {
            return buyStock(figi, limitOrderRequest);
        }
        if (limitOrderRequest.getOperation() == OperationType.SELL) {
            return sellStock(figi, limitOrderRequest);
        }
        return null;
    }

    @Override
    public void close() throws IOException {

    }

    @Override
    public String getBrokerAccountId() {
        return brokerAccountId;
    }

    @Override
    public Map<String, Boolean> hasNoDividends(List<String> figis) {
        return figis.stream().collect(Collectors.toMap(Function.identity(), x -> true));
    }

    public OffsetDateTime getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(OffsetDateTime currentTime) {
        this.currentTime = currentTime;
    }

    private PlacedLimitOrderDto buyStock(String figi, LimitOrderRequestDto limitOrderRequest) {
        BigDecimal lots = limitOrderRequest.getLots();
        BigDecimal price = limitOrderRequest.getPrice();

        BoughtStock stock = new BoughtStock();
        stock.setFigi(figi);
        stock.setPrice(price);
        stock.setLots(lots);
        stock.setCurrentBoughtId(currentBoughtId);
        stock.setType(Type.BUY);
        stock.setTime(currentTime.toLocalDateTime());

        currentBoughtId = UUID.randomUUID();

        boughtStock = stock;

        bought.add(stock);

        BigDecimal sum = lots.multiply(price);

        balance = balance.subtract(sum.subtract(sum.multiply(COMMISSION)));

        return PlacedLimitOrderDto.builder()
                .orderId("1234566789")
                .status(OrderStatus.FILL)
                .build();
    }

    private PlacedLimitOrderDto sellStock(String figi, LimitOrderRequestDto limitOrderRequest) {
        BigDecimal lots = limitOrderRequest.getLots();
        BigDecimal price = limitOrderRequest.getPrice();

        BoughtStock stock = new BoughtStock();
        stock.setFigi(figi);
        stock.setPrice(price);
        stock.setLots(lots);
        stock.setType(Type.SELL);
        stock.setCurrentBoughtId(currentSoldId);
        stock.setTime(currentTime.toLocalDateTime());

        currentSoldId = currentBoughtId;

        boughtStock = null;

        sold.add(stock);

        BigDecimal sum = lots.multiply(price);

        balance = balance.add(sum.subtract(sum.multiply(COMMISSION)));


        return PlacedLimitOrderDto.builder()
                .orderId("1234566789")
                .status(OrderStatus.FILL)
                .build();
    }

    public List<BoughtStock> getBoughtStocks() {
        return bought;
    }

    public List<BoughtStock> getSoldStocks() {
        return sold;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Data
    public static class BoughtStock {
        private BigDecimal lots;
        private BigDecimal price;
        private String figi;
        private LocalDateTime time;
        private Type type;
        private UUID currentBoughtId;
    }

    public enum Type {
        BUY,
        SELL
    }
}
