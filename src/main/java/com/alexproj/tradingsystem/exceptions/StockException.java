package com.alexproj.tradingsystem.exceptions;

public class StockException extends RuntimeException {

    public StockException(String message) {
        super(message);
    }
}
