package com.alexproj.tradingsystem.exceptions;

public class IndicatorDataException extends RuntimeException {

    public IndicatorDataException() {
    }

    public IndicatorDataException(Throwable cause) {
        super(cause);
    }
}
