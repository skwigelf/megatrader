package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.BuyDecisionDto;
import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.dto.SellDecisionDto;
import com.alexproj.tradingsystem.service.dto.DecisionRequestPricesDto;
import com.alexproj.tradingsystem.service.dto.TradeDto;

import java.math.BigDecimal;
import java.util.List;

public interface DecisionService {

    SellDecisionDto sellDecision(String figi, BigDecimal lastPrice, List<DecisionRequestPricesDto> newPrices);

    BuyDecisionDto buyDecision(String figi, List<DecisionRequestPricesDto> newPrices, List<TradeDto> trades, List<CandleDto> candles80Days);
}
