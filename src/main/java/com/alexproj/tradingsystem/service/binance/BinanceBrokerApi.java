package com.alexproj.tradingsystem.service.binance;

import com.alexproj.tradingsystem.dto.*;
import com.alexproj.tradingsystem.dto.OperationTypeWithCommission;
import com.alexproj.tradingsystem.service.BrokerApi;
import com.binance.connector.client.SpotClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
@ConditionalOnProperty(prefix = "bot", name = "brokerType", havingValue = "binance", matchIfMissing = true)
@RequiredArgsConstructor
public class BinanceBrokerApi implements BrokerApi {

    @Value("${bot.figis}")
    private List<String> figis;

    @Value("${bot.currency}")
    private String currency;

    private final SpotClient spotClient;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private List<Map<String, Object>> coinsFlat = new ArrayList<>();

    //ОСТОРОЖНО! Выставлять true при каждом запросе которое МОЖЕТ повляить на изменение баланса
    private boolean isBalanceNeededToUpdate = false;
    private Map<String, BigDecimal> coinsBalance = new ConcurrentHashMap<>();

    @Override
    public BigDecimal getCurrencyBalance(String brokerAccountId, CurrencyType currencyType) {
//        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
//        parameters.put("timestamp", new Timestamp(System.currentTimeMillis()));
//
//        String result = spotClient.createTrade().account(parameters);
//
//        Map<String, Object> resultParsed = parseJson(result);
//
//        List<Map<String, Object>> balances = (List) resultParsed.get("balances");

//        balances.forEach(balance -> {
//            coinsBalance.put("", "");
//        });

        if (isBalanceNeededToUpdate || coinsBalance.get(currencyType.name().toLowerCase()) == null) {

            fetchAllCoinsInfo();
            isBalanceNeededToUpdate = false;
        }
        //}
        return coinsBalance.get(currencyType.name().toLowerCase());
//        return coinsFlat.stream()
//                .filter(obj -> obj.get("coin").equals(currencyType.name()))
//                .map(obj -> obj.get("free"))
//                .findFirst().map(coin -> new BigDecimal((String) coin)).orElseThrow(() -> new RuntimeException("CANNot"));
    }

    @Override
    public List<String> getFigisNames(List<String> figis) {
        return figis;
    }

//    @Override
//    public List<PortfolioPositionDto> getPositions(String brokerAccountId) {
//        if (isBalanceNeededToUpdate) {
//            fetchAllCoinsInfo();
//            isBalanceNeededToUpdate = false;
//        }
//
//        return coinsBalance.entrySet().stream()
//                .filter(entry -> !figis.contains(entry.getKey()))
//                .map(x -> PortfolioPositionDto.builder()
//                        .lots(x.getValue())
//                        .figi(x.getKey())
//                        .time(LocalDateTime.now())
//                        .build()
//                ).collect(Collectors.toList());
//    }

    @Override
    public PortfolioPositionDto getPortfolioPosition(String figi, SearchMarketInstrumentDto instrument, String brokerAccountId) {

        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
        parameters.put("symbol", figi.toUpperCase());
        parameters.put("timestamp", new Timestamp(System.currentTimeMillis()));
        parameters.put("limit", 1);

        String result = spotClient.createTrade().getOrders(parameters);

        List<Map<String, Object>> resultParsed = parseJson(result);

        return resultParsed.stream()
                .map(BinanceMapper::mapToPortfolioPosition).max(Comparator.comparing(PortfolioPositionDto::getTime)).orElse(null);
    }

    @Override
    public List<OrderDto> getActiveOrders(String brokerAccountId, String figi) {
        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
        parameters.put("symbol", figi.toUpperCase());
        parameters.put("timestamp", new Timestamp(System.currentTimeMillis()));

        String result = spotClient.createTrade().getOpenOrders(parameters);

        List<Map<String, Object>> resultParsed = parseJson(result);

        List<OrderDto> orderDtos = resultParsed.stream()
                .map(BinanceMapper::mapTo).collect(Collectors.toList());

        if (orderDtos.isEmpty()) {
            isBalanceNeededToUpdate = true;
        }
        return orderDtos;
    }

    @Override
    public OrderDto getActiveOrder(String brokerAccountId, String figi, String orderId) {
        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
        parameters.put("symbol", figi.toUpperCase());
        parameters.put("timestamp", new Timestamp(System.currentTimeMillis()));
        parameters.put("orderId", orderId);

        String result = spotClient.createTrade().getOrders(parameters);

        List<Map<String, Object>> resultParsed = parseJson(result);

        List<OrderDto> orderDtos = resultParsed.stream().filter(order -> order.get("status").equals("NEW") || order.get("status").equals("PARTIALLYFILL"))
                .map(BinanceMapper::mapTo).collect(Collectors.toList());

        if (orderDtos.isEmpty()) {
            isBalanceNeededToUpdate = true;
            return null;
        }
        return orderDtos.get(0);
    }

    @Override
    public SearchMarketInstrumentDto searchByFigi(String figi) {
        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
        parameters.put("symbol", figi.toUpperCase());

        String result = spotClient.createMarket().exchangeInfo(parameters);
        //coinsFlat.stream().filter()
        Map<String, Object> resultParsed = parseJson(result);

        List<Map<String, Object>> symbols = (List<Map<String, Object>>) resultParsed.get("symbols");
        if (symbols.isEmpty()) {
            return null;
        }
        Map<String, Object> symbolInfo = symbols.stream().findFirst().orElseThrow(() -> new RuntimeException("Cannot find instruments by symbol " + figi));
        List<Map<String, Object>> filters = ((List<Map<String, Object>>) symbolInfo.get("filters"));
        BigDecimal quantity = filters.stream()
                .filter(filter -> ((String) filter.get("filterType")).equals("LOT_SIZE"))
                .map(filter -> new BigDecimal((String) filter.get("minQty")))
                .map(not -> {
                    int scale = not.precision();
                    BigDecimal notScaled = roundScaleToValue(not);//not.setScale(scale + 1, RoundingMode.HALF_UP);
                    return notScaled;
                })
                .findFirst().orElse(null);
        BigDecimal minNotional = filters.stream()
                .filter(filter -> ((String) filter.get("filterType")).equals("MIN_NOTIONAL"))
                .map(filter -> new BigDecimal((String) filter.get("minNotional")))
                .map(not -> {
                    int scale = not.precision();
                    BigDecimal notScaled = roundScaleToValue(not);//not.setScale(scale + 1, RoundingMode.HALF_UP);
                    return notScaled;
                })
                .findFirst().orElse(null);
        return SearchMarketInstrumentDto.builder()
                .name(figi)
                .figi(figi)
                .lot(quantity)
                .minNotional(minNotional)
                .minPriceIncrement(quantity)
                .build();
    }

    private BigDecimal roundScaleToValue(BigDecimal value) {
        String valueString = value.toString();

        int scale = 0;

        for (int i = valueString.indexOf("."), j = 0; i < valueString.length(); i++, j++) {
            if (valueString.charAt(i) != '0') {
                scale = j;
            }
        }
        return value.setScale(scale, RoundingMode.HALF_UP);
    }

    @Override
    public OperationDto getLastPositionLastDay(String figi, String brokerAccountId, OperationTypeWithCommission type) {
        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
        parameters.put("symbol", figi);
        parameters.put("timestamp", new Timestamp(System.currentTimeMillis()));

        String result = spotClient.createTrade().getOrders(parameters);

        List<Map<String, Object>> resultParsed = parseJson(result);

        return resultParsed.stream().filter(order -> order.get("status").equals("FILLED"))
                .map(BinanceMapper::mapToOperationDto)
                .filter(order -> order.getOperationType() == type)
                .findFirst().orElse(null);
    }

    @Override
    public void cancelOrder(String brokerAccountId, String figi, String id) {
        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
        parameters.put("symbol", figi.toUpperCase());
        parameters.put("timestamp", new Timestamp(System.currentTimeMillis()));
        parameters.put("orderId", Long.valueOf(id));

        spotClient.createTrade().cancelOrder(parameters);
    }

    @Override
    public PlacedLimitOrderDto placeLimitOrder(String brokerAccountId, String figi, LimitOrderRequestDto limitOrderRequest) {
        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
        parameters.put("symbol", figi.toUpperCase());
        parameters.put("side", limitOrderRequest.getOperation().name());
        parameters.put("type", "limit");
        parameters.put("quantity", limitOrderRequest.getLots());
        parameters.put("timeInForce", "GTC");
        parameters.put("price", limitOrderRequest.getPrice());

        String result = spotClient.createTrade().newOrder(parameters);

        return BinanceMapper.mapToPlacedLimitOrder(parseJson(result));
    }

    @Override
    public void close() throws IOException {

    }

    @Override
    public String getBrokerAccountId() {
        return "";
    }

    @Override
    public Map<String, Boolean> hasNoDividends(List<String> figis) {
        return figis.stream().collect(Collectors.toMap(figi -> figi, figi -> true));
    }

    private void fetchAllCoinsInfo() {
        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
        parameters.put("timestamp", new Timestamp(System.currentTimeMillis()));

        String result = spotClient.createWallet().coinInfo(parameters);

        List<Object> list = parseJson(result);
//        try {
//            list = objectMapper.readValue(result, new TypeReference<List<Object>>() {
//            });
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//            throw new RuntimeException("Cannot parse binance json response");
//        }

        //if (coinsFlat.isEmpty()) {
        coinsFlat = list.stream().map(obj -> ((Map<String, Object>) obj)).collect(Collectors.toList());

        List<String> figisAndCurrency = new ArrayList<>(figis);
        figisAndCurrency.add(currency.toLowerCase());

        coinsBalance = coinsFlat.stream()
                .filter(obj -> figisAndCurrency.contains(((String) obj.get("coin")).toLowerCase()))
                .collect(Collectors.toMap(obj -> ((String) obj.get("coin")).toLowerCase(), obj -> new BigDecimal((String) obj.get("free"))));
    }

    private <T> T parseJson(String json) {
        try {
            return objectMapper.readValue(json, new TypeReference<T>() {
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot parse binance json response");
        }
    }
}
