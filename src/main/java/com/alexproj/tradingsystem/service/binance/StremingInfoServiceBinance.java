package com.alexproj.tradingsystem.service.binance;

import com.alexproj.tradingsystem.dto.streaming.StreamingSubscriptionAction;
import com.alexproj.tradingsystem.service.StreamingInfoService;
import com.alexproj.tradingsystem.service.TradingProcessor;
import com.alexproj.tradingsystem.service.dto.PriceDto;
import com.binance.connector.client.WebsocketClient;
import com.binance.connector.client.utils.WebSocketCallback;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.grpc.stub.StreamObserver;
import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import ru.tinkoff.piapi.contract.v1.MarketDataRequest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "bot", name = "brokerType", havingValue = "binance", matchIfMissing = true)
@Service
public class StremingInfoServiceBinance implements StreamingInfoService {
    private final WebsocketClient websocketClient;

    @Override
    public void subscribeMarketData(List<String> figis, String brokerAccountId, Map<String, TradingProcessor> processors) {
        WebSocketCallback onOpenCallback = (message) -> {
        };

        figis.forEach(figi -> {
            websocketClient.symbolTicker(figi,
                    (message) -> {
                    },
                    ((event) -> {
                        TickerDto tickerDto = mapTo(event);

                        System.out.println("Symbol: " + tickerDto.getS());
                        System.out.println("Ask: " + tickerDto.getA());
                        System.out.println("Bid: " + tickerDto.getB());
                        System.out.println(Thread.currentThread().getName());
                        TradingProcessor tradingProcessor = processors.get(figi);

                        try {
                            tradingProcessor.process(new PriceDto(null, tickerDto.getA(), tickerDto.getB(), null), "");
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }),
                    (message) -> {
                        System.out.println("ВЫКЛЮЧЕНИЕ");
                    },
                    (error) -> {});
        });

    }

    @Override
    public MarketDataRequest createSubscribeCandlesRequest(List<String> figis, StreamingSubscriptionAction action) {
        return null;
    }

    private TickerDto mapTo(String jsonResponse) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            Map<String, Object> responseObjMap = objectMapper.readValue(jsonResponse, new TypeReference<Map<String, Object>>() {
            });

            return mapTo(responseObjMap);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot parse binance json response");
        }
    }

    private TickerDto mapTo(Map<String, Object> resp) {
        TickerDto tickerDto = new TickerDto();
        tickerDto.setA(new BigDecimal((String) resp.get("a")));
        tickerDto.setB(new BigDecimal((String) resp.get("b")));
        tickerDto.setS((String) resp.get("s"));
        return tickerDto;
    }

    @Data
    @NoArgsConstructor
    class TickerDto {
        private String s;
        private BigDecimal b;
        private BigDecimal a;
    }
}
