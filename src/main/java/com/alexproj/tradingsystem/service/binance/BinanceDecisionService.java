package com.alexproj.tradingsystem.service.binance;

import com.alexproj.tradingsystem.dto.BuyDecisionDto;
import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.dto.SellDecisionDto;
import com.alexproj.tradingsystem.service.DecisionService;
import com.alexproj.tradingsystem.service.dto.DecisionRequestPricesDto;
import com.alexproj.tradingsystem.service.dto.TradeDirection;
import com.alexproj.tradingsystem.service.dto.TradeDto;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;

@Service
@ConditionalOnProperty(prefix = "bot", name = "strategy", havingValue = "binance")
public class BinanceDecisionService implements DecisionService {

    private final static int LAST_NUMBER = 40;
    @Override
    public SellDecisionDto sellDecision(String figi, BigDecimal buyPrice, List<DecisionRequestPricesDto> newPrices) {

        if (newPrices.isEmpty()) {
            return new SellDecisionDto(false, buyPrice);
        }

        BigDecimal lastPrice = newPrices.get(newPrices.size() - 1).getPrice();

        BigDecimal percentageDifference = lastPrice.subtract(buyPrice).divide(lastPrice, 5, RoundingMode.HALF_UP);
        if (percentageDifference.compareTo(valueOf(0.01)) > 0) {
            return new SellDecisionDto(true, buyPrice);
        }
        return new SellDecisionDto(false, buyPrice);
    }

    @Override
    public BuyDecisionDto buyDecision(String figi, List<DecisionRequestPricesDto> newPrices, List<TradeDto> trades, List<CandleDto> candles80Days) {
        BigDecimal maxPrice = newPrices.stream().max(Comparator.comparing(DecisionRequestPricesDto::getPrice)).map(DecisionRequestPricesDto::getPrice).orElse(ZERO);
        BigDecimal minPrice = newPrices.stream().min(Comparator.comparing(DecisionRequestPricesDto::getPrice)).map(DecisionRequestPricesDto::getPrice).orElse(ZERO);


        if (newPrices.size() < 2 || maxPrice.compareTo(ZERO) == 0 || minPrice.compareTo(ZERO) == 0) {
            return new BuyDecisionDto(null, false);
        }

        BigDecimal lastPrice = newPrices.get(newPrices.size() - 1).getPrice();
        BigDecimal percentageDifference = maxPrice.subtract(minPrice).divide(maxPrice, 5, RoundingMode.HALF_UP);
        boolean isPricesHigh = true;

        if (newPrices.size() > LAST_NUMBER) {
            List<DecisionRequestPricesDto> pricesLast = newPrices.subList(Math.max(0, newPrices.size() - LAST_NUMBER), newPrices.size());

            BigDecimal minPriceLast = pricesLast.stream().min(Comparator.comparing(DecisionRequestPricesDto::getPrice)).map(DecisionRequestPricesDto::getPrice).orElse(ZERO);
            BigDecimal percentageDifferenceLast = lastPrice.subtract(minPriceLast).divide(lastPrice, 5, RoundingMode.HALF_UP);

            //isPricesHigh = percentageDifferenceLast.compareTo(valueOf(0.001)) > 0;
        }

        boolean isPricesLow = percentageDifference.compareTo(valueOf(0.01)) > 0 && minPrice.compareTo(newPrices.get(newPrices.size() - 1).getPrice()) == 0;

        if (isPricesLow && isPricesHigh) {
            return new BuyDecisionDto(null, true);
        }

        return new BuyDecisionDto(null, false);
    }
}
