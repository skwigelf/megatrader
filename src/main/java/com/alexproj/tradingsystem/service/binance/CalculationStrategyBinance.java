package com.alexproj.tradingsystem.service.binance;

import com.alexproj.tradingsystem.dto.CurrencyType;
import com.alexproj.tradingsystem.dto.PortfolioPositionDto;
import com.alexproj.tradingsystem.dto.SearchMarketInstrumentDto;
import com.alexproj.tradingsystem.service.BrokerApi;
import com.alexproj.tradingsystem.service.CalculationStrategy;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
@ConditionalOnProperty(prefix = "bot", name = "roundLotsMode", havingValue = "notRound", matchIfMissing = true)
@RequiredArgsConstructor
public class CalculationStrategyBinance implements CalculationStrategy {
    private static final BigDecimal LOWER_COEFFICIENT = BigDecimal.valueOf(1.01);
    private static final BigDecimal COMMISION = BigDecimal.valueOf(0.001);

    @Value("${bot.currency}")
    private String currency;

    private final BrokerApi brokerApi;

    @Override
    public BigDecimal calculateQuantity(SearchMarketInstrumentDto instrument, String brokerAccountId, BigDecimal price) {
        BigDecimal instrumentLots = instrument.getLot();

        BigDecimal usdts = brokerApi.getCurrencyBalance(brokerAccountId, CurrencyType.valueOf(currency));
        BigDecimal sumBuyVal = usdts.divide(price.multiply(LOWER_COEFFICIENT), instrument.getLot().scale(), RoundingMode.HALF_DOWN).setScale(instrumentLots.scale(), RoundingMode.HALF_DOWN);
        if (sumBuyVal.multiply(price).compareTo(instrument.getMinNotional()) >= 0) {
            return sumBuyVal;
        }
        return BigDecimal.ZERO;
    }

    @Override
    public PortfolioPositionDto recalculateQuantity(PortfolioPositionDto portfolio, BigDecimal price) {
        portfolio.setLots(portfolio.getLots().multiply(price)
                .multiply(BigDecimal.ONE.subtract(COMMISION)).divide(price, portfolio.getLots().scale(), RoundingMode.HALF_DOWN));
        return portfolio;
    }
}
