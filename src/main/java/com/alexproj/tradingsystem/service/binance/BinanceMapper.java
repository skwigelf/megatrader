package com.alexproj.tradingsystem.service.binance;

import com.alexproj.tradingsystem.dto.*;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Map;

@RequiredArgsConstructor
public class BinanceMapper {

    public static OrderDto mapTo(Map<String, Object> plainJson) {
        return OrderDto.builder()
                .figi((String) plainJson.get("symbol"))
                .build();
    }

    public static PortfolioPositionDto mapToPortfolioPosition(Map<String, Object> plainJson) {
        return PortfolioPositionDto.builder()
                .figi((String) plainJson.get("symbol"))
                .lots(roundScaleToValue(new BigDecimal((String) plainJson.get("origQty"))))
                .time(LocalDateTime.ofInstant(Instant.ofEpochMilli((Long) plainJson.get("updateTime")), ZoneId.of("UTC")))
                .build();
    }

    public static PlacedLimitOrderDto mapToPlacedLimitOrder(Map<String, Object> plainJson) {
        return PlacedLimitOrderDto.builder()
                .orderId((plainJson.get("orderId").toString()))
                .status(getOrderStatus((String) plainJson.get("status")))
                .build();
    }

    public static OperationDto mapToOperationDto(Map<String, Object> plainJson) {
        return OperationDto.builder()
                .figi((String) plainJson.get("symbol"))
                .price(new BigDecimal((String) plainJson.get("price")))
                .operationType(getType((String) plainJson.get("side")))
                .date(OffsetDateTime.now())
                //.date(OffsetDateTime.(Long) plainJson.get("time"))
                .build();
    }

    private static OperationTypeWithCommission getType(String type) {
        return OperationTypeWithCommission.valueOf(type);
    }

    private static OrderStatus getOrderStatus(String statusString) {
        if ("NEW".equals(statusString)) {
            return OrderStatus.NEW;
        }
        if ("FILLED".equals(statusString)) {
            return OrderStatus.FILL;
        }
        if ("CANCELED".equals(statusString)) {
            return OrderStatus.CANCELLED;
        }
        if ("REJECTED".equals(statusString)) {
            return OrderStatus.REJECTED;
        }
        return OrderStatus.NEW;
    }

    private static BigDecimal roundScaleToValue(BigDecimal value) {
        String valueString = value.toString();

        int scale = 0;

        for (int i = valueString.indexOf("."), j = 0; i < valueString.length(); i++, j++) {
            if (valueString.charAt(i) != '0') {
                scale = j;
            }
        }
        return value.setScale(scale, RoundingMode.HALF_UP);
    }
}
