package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.CandleDto;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.List;

public interface InfoService {
    List<CandleDto> getCandles(String figi, OffsetDateTime startDate, OffsetDateTime endDate, Duration duration);
}
