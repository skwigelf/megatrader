package com.alexproj.tradingsystem.service;

public enum TradingStatus {
    CHECK_BUYING("Ожидание покупки"),
    BUYING("Покупка"),
    CHECK_SELLING("Ожидание продажи"),
    SELLING("Продажа");

    final String russianName;

    TradingStatus(String russianName) {
        this.russianName = russianName;
    }
}
