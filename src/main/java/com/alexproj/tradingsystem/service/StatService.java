package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.service.dto.FullDealInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tinkoff.invest.openapi.model.rest.Currency;
import ru.tinkoff.invest.openapi.model.rest.Operation;
import ru.tinkoff.invest.openapi.model.rest.OperationTypeWithCommission;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;

import static ru.tinkoff.invest.openapi.model.rest.OperationTypeWithCommission.BUY;
import static ru.tinkoff.invest.openapi.model.rest.OperationTypeWithCommission.SELL;


@RequiredArgsConstructor
@Service
@Slf4j
public class StatService {
    //private final AccountInfoService accountInfoService;

    private final CacheService cacheService;

    public FullDealInfo waitAndGetRecentlyDealAsync(String brokerAccountId, String figi) {
        OffsetDateTime now = OffsetDateTime.now();
        Operation buyOperation = null;
        Operation sellOperation = null;
        log.info("Start waiting operation");
        //Ждем 20 минут
        int i = 0;
        while (i < 20) {
            try {
                log.info("Waiting operation number " + i);
                if (buyOperation == null) {
                    buyOperation = findOperation(brokerAccountId, figi, BUY, now);
                }
                if (sellOperation == null) {
                    sellOperation = findOperation(brokerAccountId, figi, SELL, now);
                }

                if (buyOperation != null && sellOperation != null) {
                    return FullDealInfo.builder()
                            .buyPrice(buyOperation.getPrice())
                            .sellPrice(sellOperation.getPrice())
                            .figi(figi)
                            .commission(buyOperation.getCommission().getValue().add(sellOperation.getCommission().getValue()))
                            .name(cacheService.getMarketInstrument(figi).getName())
                            .startTime(buyOperation.getDate())
                            .endTime(sellOperation.getDate())
                            .build();
                }
                Thread.sleep(30000);
                i++;
            } catch (InterruptedException e) {
                log.error(e.getMessage());
                e.printStackTrace();
            }
        }
        return null;
    }

    private Operation findOperation(String brokerAccountId, String figi, OperationTypeWithCommission type, OffsetDateTime now) {

        List<Operation> operations = Collections.emptyList();
        //List<Operation> operations = accountInfoService.getHistoryEntries(brokerAccountId, type, figi, now.minusMinutes(30), now, Currency.RUB);

        if (!operations.isEmpty()) {
            return operations.get(operations.size() - 1);
        }
        return null;
    }

    public Map<String, List<FullDealInfo>> getDayDeals(String brokerAccountId, OffsetDateTime date) {
        OffsetDateTime nowStartTime = OffsetDateTime.of(date.getYear(), date.getMonthValue(), date.getDayOfMonth(),
                0, 0, 0, 0, ZoneOffset.UTC);

        OffsetDateTime tomorrow = date.plusDays(1);
        OffsetDateTime nowEndTime = OffsetDateTime.of(tomorrow.getYear(), tomorrow.getMonthValue(), tomorrow.getDayOfMonth(),
                0, 0, 0, 0, ZoneOffset.UTC);

        return createHistoricEntriesMap(brokerAccountId, nowStartTime, nowEndTime);
    }

    public Map<String, List<FullDealInfo>> createHistoricEntriesMap(String brokerAccountId, OffsetDateTime start,
                                                                    OffsetDateTime end) {

        //TODO ПЕЕРЕХАТЬ НА НОВОЕ АПИИ!!!
        List<Operation> operations = Collections.emptyList();
//        List<Operation> operations = accountInfoService.getHistoryEntries(brokerAccountId,
//                null, null,
//                start, end, Currency.RUB);

        operations.sort(Comparator.comparing(Operation::getDate));

        Map<String, List<FullDealInfo>> historicEntriesMap = new HashMap<>();

        for (Operation op : operations) {
            OperationTypeWithCommission type = op.getOperationType();
            String figi = op.getFigi();
            if (type != BUY && type != SELL) {
                continue;
            }

            List<FullDealInfo> dealsByFigi = getListAndInitIfNull(historicEntriesMap, figi);

            FullDealInfo lastFullDealInfoElement = dealsByFigi.isEmpty()
                    ? null
                    : dealsByFigi.get(dealsByFigi.size() - 1);

            if (lastFullDealInfoElement != null && lastFullDealInfoElement.getLotBalance() != 0) {

                int sumLots = type == SELL
                        ? lastFullDealInfoElement.getLotBalance() + op.getQuantityExecuted()
                        : lastFullDealInfoElement.getLotBalance() - op.getQuantityExecuted();

                if (type == SELL) {
                    lastFullDealInfoElement.setEndTime(op.getDate());
                    lastFullDealInfoElement.setSellPrice(lastFullDealInfoElement.getSellPrice().add(op.getPayment()));
                }
                if (type == BUY) {
                    lastFullDealInfoElement.setStartTime(op.getDate());
                    lastFullDealInfoElement.setBuyPrice(lastFullDealInfoElement.getBuyPrice().add(op.getPayment()));
                }
                lastFullDealInfoElement.setCommission(lastFullDealInfoElement.getCommission().add(op.getCommission().getValue()));
                lastFullDealInfoElement.setLotBalance(sumLots);
            } else if (type == BUY) {

                FullDealInfo info = FullDealInfo.builder()
                        .figi(figi)
                        .lotBalance(-op.getQuantityExecuted())
                        .startTime(op.getDate())
                        .buyPrice(BigDecimal.ZERO)
                        .sellPrice(BigDecimal.ZERO)
                        .commission(op.getCommission().getValue())
                        .name(cacheService.getMarketInstrument(figi).getName())
                        .buyPrice(op.getPayment())
                        .build();

                dealsByFigi.add(info);
                historicEntriesMap.put(figi, dealsByFigi);
            }
        }

        return historicEntriesMap;
    }

    private List<FullDealInfo> getListAndInitIfNull(Map<String, List<FullDealInfo>> map, String figi) {
        List<FullDealInfo> dealsByFigi = map.get(figi);
        if (dealsByFigi == null) {
            dealsByFigi = new ArrayList<>();
        }

        return dealsByFigi;
    }
}
