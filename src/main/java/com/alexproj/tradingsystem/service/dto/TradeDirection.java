package com.alexproj.tradingsystem.service.dto;

public enum TradeDirection {
    TRADE_DIRECTION_UNSPECIFIED,
    TRADE_DIRECTION_BUY,
    TRADE_DIRECTION_SELL
}
