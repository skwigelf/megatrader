package com.alexproj.tradingsystem.service.dto;

import com.alexproj.tradingsystem.dto.SearchMarketInstrumentDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.tinkoff.invest.openapi.model.rest.SearchMarketInstrument;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
public class IndicatorDto {
    private SearchMarketInstrumentDto marketInstrument;
    private BigDecimal bollingerDown;
    private BigDecimal rsi;
    private BigDecimal price;
    private List<TradeDto> trades;
}