package com.alexproj.tradingsystem.service.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class ShortDealSum {
    private BigDecimal profit;
    private BigDecimal profitPercentage;
    private String name;
    private String figi;
    private BigDecimal commission;
    private int numberOfDeals;
}