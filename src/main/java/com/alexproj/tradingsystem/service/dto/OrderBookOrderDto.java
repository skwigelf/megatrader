package com.alexproj.tradingsystem.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@AllArgsConstructor
@Data
public class OrderBookOrderDto {
    private BigDecimal price;
    private long quantity;
}
