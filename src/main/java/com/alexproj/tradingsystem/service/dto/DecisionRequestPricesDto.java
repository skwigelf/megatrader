package com.alexproj.tradingsystem.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class DecisionRequestPricesDto {
    private BigDecimal price;
    private LocalDateTime time;
}
