package com.alexproj.tradingsystem.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class PriceDto {
    private BigDecimal price;
    private BigDecimal ask;
    private BigDecimal bid;
    private LocalDateTime time;
}
