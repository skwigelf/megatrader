package com.alexproj.tradingsystem.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@AllArgsConstructor
@Data
public class TradeDto {
    private long quantity;
    private TradeDirection direction;
    private BigDecimal price;
    private OffsetDateTime time;
}
