package com.alexproj.tradingsystem.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

@AllArgsConstructor
@Data
public class OrderBookDto {
    private int depth;
    private boolean isConsistent;
    private List<OrderBookOrderDto> bids;
    private List<OrderBookOrderDto> asks;
    private OffsetDateTime time;

    private BigDecimal limitUp;
    private BigDecimal limitDown;
}
