package com.alexproj.tradingsystem.service.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@Builder
public class FullDealInfo {
    private OffsetDateTime startTime;
    private OffsetDateTime endTime;
    private String figi;
    private String name;
    private BigDecimal buyPrice;
    private BigDecimal sellPrice;
    private BigDecimal commission;

    private int lotBalance;
}
