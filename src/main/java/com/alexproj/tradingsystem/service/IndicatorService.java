package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.exceptions.IndicatorDataException;
import com.alexproj.tradingsystem.repo.PythonIndicatorRepository;
import com.alexproj.tradingsystem.repo.Ta4jIndicatorRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@AllArgsConstructor
public class IndicatorService {

    private final PythonIndicatorRepository pythonIndicatorRepository;

    private final Ta4jIndicatorRepository ta4jIndicatorRepository;

    public BigDecimal bollingerLow(List<CandleDto> candles) {
        try {
            return ta4jIndicatorRepository.bollingerLow(candles, 20);
        } catch (Exception e) {
            throw new IndicatorDataException(e);
        }
    }

    public BigDecimal rsi(List<CandleDto> candles) {
        try {
            return ta4jIndicatorRepository.rsi(candles, 14);
        } catch (Exception e) {
            throw new IndicatorDataException(e);
        }
    }
}
