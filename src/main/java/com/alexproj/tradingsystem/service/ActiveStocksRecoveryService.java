package com.alexproj.tradingsystem.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service
@RequiredArgsConstructor
public class ActiveStocksRecoveryService {
    public static final String LAST_FIGI_FILENAME = "lastFigi.txt";

    public List<String> readFigis() {
        File file = new File(LAST_FIGI_FILENAME);
        try (InputStream inputStream = new FileInputStream(file)) {
            String figisString = new String(inputStream.readAllBytes(), UTF_8);

            return Arrays.stream(figisString.split(",\\s*")).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка во время чтения сохраненных акций из файла () ");
        }
    }

    public void writeFigi(String figi) throws IOException {
        List<String> figis = readFigis();
        if (figis.contains(figi)) {
            return;
        }
        figis.add(figi);
        File file = new File(LAST_FIGI_FILENAME);
        try (OutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(String.join(",", figis).getBytes());
        }
    }

    public void deleteFigi(String figi) throws IOException {
        List<String> figis = readFigis();
        if (!figis.contains(figi)) {
            return;
        }
        figis.remove(figi);
        File file = new File(LAST_FIGI_FILENAME);
        try (OutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(String.join(",", figis).getBytes());
        }
    }

    public void deleteAll() throws IOException {
        File file = new File(LAST_FIGI_FILENAME);
        try (OutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write("".getBytes());
        }
    }
}
