package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.service.dto.FullDealInfo;
import com.alexproj.tradingsystem.service.dto.ShortDealSum;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.alexproj.tradingsystem.util.ExcelUtils.createCell;

@Service
@RequiredArgsConstructor
public class ReportService {

    private final StatService statService;

    private final BrokerApi api;

    private CellStyle redStyle;
    private CellStyle greenStyle;

    @SneakyThrows
    public void createReport() {
        Workbook workbook = new XSSFWorkbook();
        workbook.createSheet("Итог");

        redStyle = workbook.createCellStyle();
        redStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        redStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        greenStyle = workbook.createCellStyle();
        greenStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        greenStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        OffsetDateTime startDate = OffsetDateTime.of(2021, 12, 10, 0, 0, 0, 0, ZoneOffset.UTC);
        OffsetDateTime startDateDecember = OffsetDateTime.of(2021, 11, 9, 12, 0, 0, 0, ZoneOffset.UTC);

        OffsetDateTime endDate = OffsetDateTime.now();

        String brokerAccountId = api.getBrokerAccountId();

        Map<String, List<FullDealInfo>> historicEntries = statService.createHistoricEntriesMap(brokerAccountId,
                startDateDecember, endDate);

        createFullResultSheet(workbook, historicEntries);
        createStocksSheet(workbook, historicEntries);

        File file = new File("otchetDec.xlsx");
        try (OutputStream outputStream = new FileOutputStream(file)) {
            workbook.write(outputStream);
        }

        workbook.close();
    }

    private void createFullResultSheet(Workbook workbook, Map<String, List<FullDealInfo>> historicEntries) {
        Sheet sheet = workbook.createSheet("Все записи");

        createCell(sheet, 1, 0, "Название акции");
        createCell(sheet, 0, 1, "Покупка");
        createCell(sheet, 0, 2, "Продажа");

        sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 4, 7));

        createCell(sheet, 1, 1, "Дата и время покупки");
        createCell(sheet, 1, 2, "Сумма покупки");

        createCell(sheet, 1, 3, "Дата и время продажи");
        createCell(sheet, 1, 4, "Сумма продажи");
        createCell(sheet, 1, 5, "Комиссия");

        createCell(sheet, 1, 6, "Профит (в рублях)");
        createCell(sheet, 1, 7, "Профит (в процентах)");

        List<FullDealInfo> historicEntriesSortedByStartDate = historicEntries.values().stream()
                .flatMap(Collection::stream)
                .filter(historicEntry -> !historicEntry.getName().equals("Татнефть"))
                .filter(historicEntry -> historicEntry.getSellPrice().compareTo(BigDecimal.valueOf(5000)) > 0)
                .sorted(Comparator.comparing(FullDealInfo::getStartTime))
                .collect(Collectors.toList());

        for (int i = 0; i < historicEntriesSortedByStartDate.size(); i++) {
            fillFullResultRow(sheet, i + 2, historicEntriesSortedByStartDate.get(i));
        }
    }

    private void createStocksSheet(Workbook workbook, Map<String, List<FullDealInfo>> historicEntries) {
        Sheet sheet = workbook.createSheet("Записи по акциям");

        createCell(sheet, 0, 0, "Название акции");
        createCell(sheet, 0, 1, "Количество сделок");
        createCell(sheet, 0, 2, "Профит");
        createCell(sheet, 0, 3, "Комиссия");
        createCell(sheet, 0, 4, "Профит (в процентах)");

        Map<String, ShortDealSum> mapa = historicEntries.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> {
                            BigDecimal sum = entry.getValue().stream()
                                    .map(x -> x.getBuyPrice().add(x.getSellPrice()))
                                    .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                            BigDecimal commission = entry.getValue().stream()
                                    .map(FullDealInfo::getCommission)
                                    .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                            BigDecimal sumBuy = entry.getValue().stream()
                                    .map(FullDealInfo::getBuyPrice)
                                    .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                            BigDecimal sumSell = entry.getValue().stream()
                                    .map(FullDealInfo::getSellPrice)
                                    .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                            BigDecimal profitPercentage = (sumSell.add(sumBuy))
                                    .divide(BigDecimal.valueOf(150000), 5, RoundingMode.HALF_UP)
                                    .multiply(BigDecimal.valueOf(100));

                            return ShortDealSum.builder()
                                    .figi(entry.getKey())
                                    .name(entry.getValue().get(0).getName())
                                    .numberOfDeals(entry.getValue().size())
                                    .profit(sum)
                                    .commission(commission)
                                    .profitPercentage(profitPercentage)
                                    .build();
                        }
                ));

        int i = 0;
        for (ShortDealSum deal : mapa.values()) {
            fillStockRow(sheet, i + 1, deal);
            i++;
        }
    }

    private void fillStockRow(Sheet sheet, int row, ShortDealSum deal) {
        createCell(sheet, row, 0, deal.getName());
        createCell(sheet, row, 1, deal.getNumberOfDeals());
        Cell profit = createCell(sheet, row, 2, deal.getProfit());
        Cell commission = createCell(sheet, row, 3, deal.getCommission());
        Cell profitPercentage = createCell(sheet, row, 4, deal.getProfitPercentage());
        if (deal.getProfitPercentage().compareTo(BigDecimal.ZERO) < 0) {
            profit.setCellStyle(redStyle);
            profitPercentage.setCellStyle(redStyle);
        }
        if (deal.getProfitPercentage().compareTo(BigDecimal.ZERO) > 0) {
            profit.setCellStyle(greenStyle);
            profitPercentage.setCellStyle(greenStyle);
        }
    }

    private void fillFullResultRow(Sheet sheet, int row, FullDealInfo fullDealInfo) {
        createCell(sheet, row, 0, fullDealInfo.getName());
        createCell(sheet, row, 1, fullDealInfo.getStartTime());
        createCell(sheet, row, 2, fullDealInfo.getBuyPrice());
        createCell(sheet, row, 3, fullDealInfo.getEndTime());
        createCell(sheet, row, 4, fullDealInfo.getSellPrice());
        createCell(sheet, row, 5, fullDealInfo.getCommission());
        BigDecimal profit = fullDealInfo.getSellPrice().add(fullDealInfo.getBuyPrice());
        BigDecimal profitPercentage =
                (fullDealInfo.getSellPrice().add(fullDealInfo.getBuyPrice()))
                        .divide(fullDealInfo.getBuyPrice(), 5, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(-1));
        Cell profitCell = createCell(sheet, row, 6, profit);
        Cell profitPercentageCell = createCell(sheet, row, 7, profitPercentage.multiply(BigDecimal.valueOf(100)));
        if (profit.compareTo(BigDecimal.ZERO) < 0) {
            profitCell.setCellStyle(redStyle);
            profitPercentageCell.setCellStyle(redStyle);
        }
        if (profit.compareTo(BigDecimal.ZERO) > 0) {
            profitCell.setCellStyle(greenStyle);
            profitPercentageCell.setCellStyle(greenStyle);
        }
    }



}
