package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.PortfolioPositionDto;
import com.alexproj.tradingsystem.dto.SearchMarketInstrumentDto;

import java.math.BigDecimal;

public interface CalculationStrategy {
    BigDecimal calculateQuantity(SearchMarketInstrumentDto instrument, String brokerAccountId, BigDecimal price);

    PortfolioPositionDto recalculateQuantity(PortfolioPositionDto portfolio, BigDecimal price);
}
