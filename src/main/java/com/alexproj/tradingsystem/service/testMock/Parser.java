package com.alexproj.tradingsystem.service.testMock;

import com.alexproj.tradingsystem.dto.CandleDto;
import org.springframework.stereotype.Component;

import java.io.*;
import java.math.BigDecimal;
import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class Parser {

    private static Map<String, String> figiNames = Map.of("RUAL", "BBG008F2T3T2");

    public static List<CandleDto> getCandles() throws IOException {
        File file = new File(Parser.class.getResource("/candles/RUAL3.txt").getFile());//ResourceUtils.getFile("classpath:candles/RUAL2.txt");
        System.out.println(file.exists());
        InputStream in = new FileInputStream(file);

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        List<CandleDto> tickerDtos = new ArrayList<>();

        int skip = 7_000;
        int limit = 0;
        reader.readLine();
        while (reader.ready()) {
            String[] line = reader.readLine().split(",");

            if (limit < skip) {
                limit++;
                continue;
            }

            OffsetDateTime date = createDateTime(line);
            BigDecimal open = new BigDecimal(line[4]);
            BigDecimal high = new BigDecimal(line[5]);
            BigDecimal low = new BigDecimal(line[6]);
            BigDecimal close = new BigDecimal(line[7]);
            Long vol = Long.parseLong(line[8]);

            CandleDto candleDto = CandleDto.builder()
                    .open(open)
                    .high(high)
                    .low(low)
                    .close(close)
                    .volume(vol)
                    .time(date)
                    .figi(figiNames.get(line[0]))
                    .interval(Duration.ofMinutes(1))
                    .build();


            tickerDtos.add(candleDto);
        }

        return tickerDtos;
    }

    private static OffsetDateTime createDateTime(String[] line) {
        String dateStr = line[2];
        LocalDate date = LocalDate.of(
                Integer.parseInt(dateStr.substring(0, 4)),
                Integer.parseInt(dateStr.substring(4, 6)),
                Integer.parseInt(dateStr.substring(6, 8)));
        String timeStr = line[3];
        LocalTime time = LocalTime.of(
                Integer.parseInt(timeStr.substring(0, 2)),
                Integer.parseInt(timeStr.substring(2, 4))
        );

        return OffsetDateTime.of(date, time, ZoneOffset.UTC);
    }
}
