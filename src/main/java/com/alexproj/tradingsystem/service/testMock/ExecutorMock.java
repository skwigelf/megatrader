package com.alexproj.tradingsystem.service.testMock;

import com.alexproj.tradingsystem.service.TradingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ExecutorMock {

    @Value("${bot.mock:false}")
    boolean testMode;

    @Value("${bot.figis}")
    List<String> figis;

    private final TradingService tradingService;

    public void start() {
        if (testMode) {
            try {
                tradingService.startSearchingStocks("123", figis);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("");
        }
    }
}
