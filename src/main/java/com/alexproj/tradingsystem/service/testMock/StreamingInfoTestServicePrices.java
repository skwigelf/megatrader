package com.alexproj.tradingsystem.service.testMock;

import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.dto.streaming.StreamingSubscriptionAction;
import com.alexproj.tradingsystem.service.StreamingInfoService;
import com.alexproj.tradingsystem.service.TradingProcessor;
import com.alexproj.tradingsystem.service.dto.PriceDto;
import com.alexproj.tradingsystem.testmock.BrokerApiTest;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import ru.tinkoff.piapi.contract.v1.MarketDataRequest;

import javax.annotation.PostConstruct;
import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.BiConsumer;

import static com.alexproj.tradingsystem.testmock.BrokerApiTest.COMMISSION;
import static com.alexproj.tradingsystem.util.ExcelUtils.createCell;
import static com.alexproj.tradingsystem.util.ExcelUtils.createCellFormula;

@Component
@RequiredArgsConstructor
@Slf4j
@ConditionalOnProperty(prefix = "bot", name = "mock", havingValue = "true")
public class StreamingInfoTestServicePrices implements StreamingInfoService {


    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd-hh-mm-ss");
    private List<CandleDto> parsedTicks = new ArrayList<>();


    private final BrokerApiTest brokerApi;

    private final CacheServiceMock cacheServiceMock;

    private File excelFile;

    private OutputStream fileGoodOutputStream;

    private OutputStream fileBadInputStream;

    private Workbook excelWorkbook;

    private Sheet goodSheet;

    private Sheet badSheet;

    @Value("${bot.test.fileType}")
    String fileType;

    @PostConstruct
    public void setUp() {
        try {
            parsedTicks = Parser.getCandles();
            cacheServiceMock.setParsedTicks(parsedTicks);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    private void createFile() {

        if (isExcel()) {

            excelFile = new File("statistic" + LocalDateTime.now().format(DATE_TIME_FORMATTER) + ".xlsx");
            excelWorkbook = new XSSFWorkbook();
            goodSheet = excelWorkbook.createSheet("Удачные сделки");
            badSheet = excelWorkbook.createSheet("Плохие сделки");

            fillHeader(goodSheet);
            fillHeader(badSheet);
        } else {
            File fileBad = new File("badstatistic" + LocalDateTime.now().format(DATE_TIME_FORMATTER) + ".xlsx");
            File fileGood = new File("goodstatistic" + LocalDateTime.now().format(DATE_TIME_FORMATTER) + ".xlsx");

            fileBadInputStream = new FileOutputStream(fileBad);
            fileGoodOutputStream = new FileOutputStream(fileGood);
        }
    }

    @Override
    public void subscribeMarketData(List<String> figis, String brokerAccountId, Map<String, TradingProcessor> processors) throws IOException {

        cacheServiceMock.resetCountrs();
        createFile();


        long currentTimeMillis = System.currentTimeMillis();
        figis.forEach(figi -> {
            parsedTicks.stream().skip(CacheServiceMock.STANDARD_CANDLES_NUMBER + 1).forEach((candleDto) -> {
                cacheServiceMock.updateCandlesAndCurrentTime(candleDto.getTime());

                brokerApi.setCurrentTime(candleDto.getTime());

                TradingProcessor tradingProcessor = processors.get(figi);

                tradingProcessor.process(new PriceDto(candleDto.getClose(), null, null, candleDto.getTime().toLocalDateTime()),"");
            });
        });

        List<BrokerApiTest.BoughtStock> bought = brokerApi.getBoughtStocks();
        List<BrokerApiTest.BoughtStock> sold = brokerApi.getSoldStocks();

        try {
            fillResult(bought, sold);
            if (isExcel()) {
                try (OutputStream outputStream = new FileOutputStream(excelFile)) {
                    excelWorkbook.write(outputStream);
                }

                System.out.println("Количество секунд " + ((System.currentTimeMillis() - currentTimeMillis) / 1000));

                excelWorkbook.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileBadInputStream != null) {
                fileBadInputStream.close();
            }
            if (fileGoodOutputStream != null) {
                fileGoodOutputStream.close();
            }
        }
        System.out.println(brokerApi.getBalance());
    }

    @Override
    public MarketDataRequest createSubscribeCandlesRequest(List<String> figis, StreamingSubscriptionAction action) {
        return null;
    }

    public List<CandleDto> getParsedTicks() {
        return parsedTicks;
    }

    private void fillTextFile(BrokerApiTest.BoughtStock buyOperation, BrokerApiTest.BoughtStock sellOperation, boolean isStockProfit) throws IOException {
        if (isStockProfit) {
            fileGoodOutputStream.write(("Покупка по цене: " + buyOperation.getPrice() + ", Время покупки: " +
                    buyOperation.getTime().format(DateTimeFormatter.ISO_DATE_TIME) + ", Продажа по цене: " + sellOperation.getPrice() +
                    ", Время продажи: " + sellOperation.getTime().format(DateTimeFormatter.ISO_DATE_TIME)).getBytes(StandardCharsets.UTF_8));
        } else {
            fileBadInputStream.write(("Покупка по цене: " + buyOperation.getPrice() + ", Время покупки: " +
                    buyOperation.getTime().format(DateTimeFormatter.ISO_DATE_TIME) + ", Продажа по цене: " + sellOperation.getPrice() +
                    ", Время продажи: " + sellOperation.getTime().format(DateTimeFormatter.ISO_DATE_TIME)).getBytes(StandardCharsets.UTF_8));

        }
    }

    private void fillExcelFile(BrokerApiTest.BoughtStock buyOperation, BrokerApiTest.BoughtStock sellOperation, boolean isStockProfit) throws IOException {

        var sheetToFill = isStockProfit ? goodSheet : badSheet;

        int lastRow = sheetToFill.getLastRowNum();

        createCell(sheetToFill, lastRow + 1, 0, buyOperation.getPrice());
        createCell(sheetToFill, lastRow + 1, 1, buyOperation.getTime().format(DateTimeFormatter.ISO_DATE_TIME));
        createCell(sheetToFill, lastRow + 1, 2, sellOperation.getPrice());
        createCell(sheetToFill, lastRow + 1, 3, sellOperation.getTime().format(DateTimeFormatter.ISO_DATE_TIME));
        createCellFormula(sheetToFill, lastRow + 1, 4, "C" + (lastRow + 2) + " - A" + (lastRow + 2));
        createCellFormula(sheetToFill, lastRow + 1, 5, "((C" + (lastRow + 2) + " - A" + (lastRow + 2) + ")/C" + (lastRow + 2) + ") * 100");
    }

    private void fillHeader(Sheet sheet) {
        sheet.setColumnWidth(0, 3500);
        sheet.setColumnWidth(1, 4500);
        sheet.setColumnWidth(2, 3500);
        sheet.setColumnWidth(3, 4500);
        sheet.setColumnWidth(4, 3500);
        sheet.setColumnWidth(5, 3500);
        createCell(sheet, 0, 0, "Цена покупки");
        createCell(sheet, 0, 1, "Время покупки");
        createCell(sheet, 0, 2, "Цена продажи");
        createCell(sheet, 0, 3, "Время продажи");
        createCell(sheet, 0, 4, "Профит (абс значение)");
        createCell(sheet, 0, 5, "Профит (В процентах)");
    }

    private void fillResult(List<BrokerApiTest.BoughtStock> bought, List<BrokerApiTest.BoughtStock> sold) throws IOException {

        Map<UUID, List<BrokerApiTest.BoughtStock>> calculatedStocks = new LinkedHashMap<>();

        for (int i = 0; i < bought.size(); i++) {
            var boughtStock = bought.get(i);

            for (int j = i; j < sold.size(); j++) {
                var soltStock = sold.get(j);

                if (boughtStock.getCurrentBoughtId().equals(soltStock.getCurrentBoughtId())) {
                    var value = calculatedStocks.get(soltStock.getCurrentBoughtId());
                    if (value == null) {
                        value = new ArrayList<>();
                        value.add(boughtStock);
                    }
                    value.add(soltStock);
                    calculatedStocks.put(soltStock.getCurrentBoughtId(), value);
                    break;
                }
            }
        }

        for (var entry : calculatedStocks.entrySet()) {
            var value = entry.getValue();


            var buyOpertionOptional = value.stream()
                    .filter(x -> x.getType() == BrokerApiTest.Type.BUY)
                    .findFirst();
            var sellOperationOptional = value.stream()
                    .filter(x -> x.getType() == BrokerApiTest.Type.SELL)
                    .findFirst();

            if (buyOpertionOptional.isEmpty() || sellOperationOptional.isEmpty()) {
                return;
            }

            var buyOperation = buyOpertionOptional.get();
            var sellOperation = sellOperationOptional.get();

            boolean isStockProfit = sellOperation.getPrice().subtract(COMMISSION).subtract(COMMISSION)
                    .compareTo(buyOperation.getPrice()) >= 0;

            if (isExcel()) {
                fillExcelFile(buyOperation, sellOperation, isStockProfit);
            } else {
                fillTextFile(buyOperation, sellOperation, isStockProfit);
            }
        }
    }


    boolean isExcel() {
        return fileType.equals("excel");
    }
}
