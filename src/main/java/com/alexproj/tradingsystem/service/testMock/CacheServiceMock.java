package com.alexproj.tradingsystem.service.testMock;

import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.dto.SearchMarketInstrumentDto;
import com.alexproj.tradingsystem.service.CacheService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
@ConditionalOnExpression("${bot.mock:true}")
public class CacheServiceMock implements CacheService {

    private int candleCounter = STANDARD_CANDLES_NUMBER;

    public static int STANDARD_CANDLES_NUMBER = 200;
    private List<CandleDto> parsedTicks;

    private OffsetDateTime currentCandleTime;

    private List<CandleDto> currentCandles = new ArrayList<>();
    private Duration candlesDuration = Duration.ofMinutes(5);

    public void setParsedTicks(List<CandleDto> parsedTicks) {
        this.parsedTicks = parsedTicks;
        currentCandleTime = parsedTicks.get(0).getTime();
        currentCandles = changeDurationOfCandles(parsedTicks.subList(candleCounter - STANDARD_CANDLES_NUMBER, candleCounter), candlesDuration);
    }

    @Override
    public List<CandleDto> getCandles(String figi) {
        CandleDto lastCandle = parsedTicks.get(candleCounter);

        CandleDto lastCurrentCandle = currentCandles.get(currentCandles.size() - 1);
        if (lastCurrentCandle.getHigh().compareTo(lastCandle.getHigh()) <= 0) {
            lastCurrentCandle.setHigh(lastCandle.getHigh());
        }
        if (lastCurrentCandle.getLow().compareTo(lastCandle.getLow()) <= 0) {
            lastCurrentCandle.setLow(lastCandle.getLow());
        }
        lastCurrentCandle.setClose(lastCandle.getClose());

        return currentCandles;
    }

    public void resetCountrs() {
        candleCounter = STANDARD_CANDLES_NUMBER;
        currentCandles = new ArrayList<>();
    }

    @Override
    public SearchMarketInstrumentDto getMarketInstrument(String figi) {
        return SearchMarketInstrumentDto.builder()
                .figi(figi)
                .name(figi)
                .dlong(BigDecimal.ZERO)
                .lot(BigDecimal.ONE)
                .minPriceIncrement(BigDecimal.valueOf(0.0001))
                .minNotional(BigDecimal.valueOf(0.0001))
                .build();
    }

    public void updateCandlesAndCurrentTime(OffsetDateTime currentTime) {
        candleCounter++;
        if (currentCandleTime.plus(candlesDuration).compareTo(currentTime) <= 0) {
            currentCandleTime = currentTime;

            currentCandles = changeDurationOfCandles(parsedTicks.subList(candleCounter - STANDARD_CANDLES_NUMBER, candleCounter), candlesDuration);
        }
    }

    private List<CandleDto> changeDurationOfCandles(List<CandleDto> candles, Duration duration) {
        List<CandleDto> newCandles = new ArrayList<>();

        CandleDto firstCandle = candles.get(0);
        CandleDto highCandle = candles.get(0);
        CandleDto lowCandle = candles.get(0);
        OffsetDateTime firstTime = firstCandle.getTime();

        for (int i = 1; i < candles.size(); i++) {
            CandleDto secondCandle = candles.get(i);
            if (secondCandle.getHigh().compareTo(highCandle.getHigh()) > 0) {
                highCandle = secondCandle;
            }
            if (secondCandle.getLow().compareTo(lowCandle.getLow()) < 0) {
                lowCandle = secondCandle;
            }
            if (i + 1 >= candles.size()) {
                newCandles.add(CandleDto.builder()
                        .open(firstCandle.getOpen())
                        .high(highCandle.getHigh())
                        .low(lowCandle.getLow())
                        .close(secondCandle.getClose())
                        .time(secondCandle.getTime())
                        .volume(secondCandle.getVolume())
                        .figi(firstCandle.getFigi())
                        .interval(candlesDuration)
                        .build());
                break;
            }
            if (firstTime.compareTo(secondCandle.getTime().minus(duration)) <= 0) {

                newCandles.add(CandleDto.builder()
                        .open(firstCandle.getOpen())
                        .high(highCandle.getHigh())
                        .low(lowCandle.getLow())
                        .close(secondCandle.getClose())
                        .time(firstCandle.getTime())
                        .volume(secondCandle.getVolume())
                        .figi(firstCandle.getFigi())
                        .interval(candlesDuration)
                        .build());

                firstCandle = candles.get(i);
                highCandle = candles.get(i);
                lowCandle = candles.get(i);
                firstTime = firstCandle.getTime();
            }
        }

        return newCandles;
    }

    @Override
    public boolean hasStockInPortfolio(String figi) {
        return false;
    }
}
