package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.*;
import com.alexproj.tradingsystem.notifications.NotificationTechMessages;
import com.alexproj.tradingsystem.notifications.ServiceNotification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;

import static com.alexproj.tradingsystem.service.TradingStatus.*;
import static java.util.stream.Collectors.toList;

/**
 * Сервис может быть вынесен в микросервис - это ядро бота
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class TradingService {

    private static final BigDecimal LOWER_COEFFICIENT = BigDecimal.valueOf(100.01);

    int DEFAULT_THREAD_SLEEP_VALUE = 2000;
    private static final int CANDLES_NUMBER = 80;
    public static final BigDecimal MIN_SELL_PRICE = BigDecimal.valueOf(0.0015);

    @Value("${bot.figisExcludedInTime}")
    List<String> figisExcludedInTime;

    @Value("${bot.mock:false}")
    boolean isTestMode;

    private final Map<String, TradingProcessor> stocksProcessors = new ConcurrentHashMap<>();

    private final DecisionService decisionService;
    private final ServiceNotification telegramNotificationService;

    private final StreamingInfoService streamingInfoService;

    private final CacheService cacheService;

    private final StatService statService;

    private final CalculationStrategy calculationStrategy;

    private final TradeDbService dbService;
    private final BrokerApi brokerApi;


    @Value("${bot.buyDataType}")
    private String buyDataType;

    @Value("${bot.brokerType}")
    private String brokerType;

    private void initStatuses(List<String> figis, String brokerAccountId) {
        Predicate<TradingStatus> defaultFilter = (tradingStatus) -> true;
        Predicate<TradingStatus> filterOnEvening = (tradingStatus) -> {
            OffsetTime now = OffsetTime.now();
            return (tradingStatus == CHECK_SELLING || tradingStatus == SELLING) ||
                    (now.isAfter(OffsetTime.of(6, 59, 0, 0, ZoneOffset.of("+03:00")))
                            && now.isBefore(OffsetTime.of(17, 0, 0, 0, ZoneOffset.of("+03:00"))));
        };
        figis.forEach(figi -> {
            SearchMarketInstrumentDto inst = cacheService.getMarketInstrument(figi);
            log.info("sSTAVKA LONGA  !!!! " + " " + inst.getDlong());
            TradingProcessor tradingProcessor = new TradingProcessor(
                    cacheService,
                    decisionService,
                    telegramNotificationService,
                    this,
                    figisExcludedInTime.contains(figi) ? filterOnEvening : defaultFilter,
                    buyDataType,
                    brokerType,
                    dbService,
                    isTestMode
            );
            tradingProcessor.setFigi(figi);

            boolean stockAlreadyBought = brokerApi.getPortfolioPosition(figi, null, brokerAccountId) != null;
            if (stockAlreadyBought) {
                OperationDto operation = brokerApi.getLastPositionLastDay(figi, brokerAccountId, OperationTypeWithCommission.BUY);
                if (operation == null) {
                    tradingProcessor.setDisabled(new AtomicBoolean(true));
                    telegramNotificationService.sendNotificationTech("Акция " + cacheService.getMarketInstrument(figi).getName()
                            + " была ранее куплена, но по ней отсутствует информация в истории. Торговля этой акцией выключена");
                } else {
                    tradingProcessor.setTradingStatus(CHECK_SELLING);
                    tradingProcessor.setBuyPrice(operation.getPrice());
                }
            } else {
                tradingProcessor.setTradingStatus(CHECK_BUYING);
            }

            stocksProcessors.put(figi, tradingProcessor);
        });
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void startSearchingStocks(String brokerAccountId, List<String> figis) throws InterruptedException {
        try {
            System.out.println("STTAAAARTEED 1");
            initStatuses(figis, brokerAccountId);
            System.out.println("STTAAAARTEED 2");

            streamingInfoService.subscribeMarketData(figis, brokerAccountId, stocksProcessors);

            while (!isTestMode) {
                Thread.sleep(DEFAULT_THREAD_SLEEP_VALUE);
            }
        } catch (Exception e) {

        }

    }

    public boolean sell(String figi, String brokerAccountId, BigDecimal buyPrice, BigDecimal price) throws InterruptedException {
        log.info("Решение о продаже \n");

        SearchMarketInstrumentDto instrument = cacheService.getMarketInstrument(figi);

        PortfolioPositionDto portfolioPosition = brokerApi.getPortfolioPosition(figi, instrument, brokerAccountId);

        portfolioPosition = calculationStrategy.recalculateQuantity(portfolioPosition, price);


        BigDecimal pricePercentageDiff = price.subtract(buyPrice).divide(buyPrice, 5, RoundingMode.HALF_UP);

        //Если пришло положительное решение на продажу но при этом цена сливается не по жесткому стоп лоссу
        if (pricePercentageDiff.compareTo(MIN_SELL_PRICE) < 0) {
            //price = calculatePriceByMinSellPoint(minPriceIncrement, buyPrice);
        }

        BigDecimal orderPrice = placeLimitOrderSellAndWait(figi, brokerAccountId, portfolioPosition.getLots(), price);

        if (orderPrice != null) {
            telegramNotificationService.sendNotificationTech("Заявка на продажу исполнена" + "\n"
                    + " Текущая Цена продажи (из истории операций): " + orderPrice);

//            CompletableFuture.runAsync(() -> {
//                FullDealInfo fullDealInfo = statService.waitAndGetRecentlyDealAsync(brokerAccountId, figi);
//                if (fullDealInfo != null) {
//                    telegramNotificationService.sendNotification(NotificationMessages.SUCCESS_TRADE_MESSAGE(fullDealInfo));
//                }
//            });

            return true;
        }

        return false;
    }

    public BuyResultDto buy(SearchMarketInstrumentDto instrument, BigDecimal price, String brokerAccountId) throws InterruptedException {
        //log.info("Решение о покупке: " + true);
        log.info("Buy decision: " + true);

        String figi = instrument.getFigi();

        BigDecimal lots = calculationStrategy.calculateQuantity(instrument, brokerAccountId, price);

        if (lots.compareTo(BigDecimal.ZERO) <= 0) {
            //telegramNotificationService.sendNotification("Не хватает денег для покупки");
            //log.info("Не хватает денег для покупки " + instrument.getName());
            log.info("Not enough money to buy " + instrument.getName());
            return new BuyResultDto(false, null);
        }

        telegramNotificationService.sendNotificationTech(NotificationTechMessages.buyDecisionMessage(instrument.getName(), price));

        BigDecimal orderPrice = placeLimitOrderBuyAndWait(figi, brokerAccountId, price, lots);
        if (orderPrice == null) {
            return new BuyResultDto(false, null);
        }

        telegramNotificationService.sendNotificationTech("Заявка на покупку исполнена " + "\n"
                + "Цена за шт: " + orderPrice + "\n");

        return new BuyResultDto(true, orderPrice);
    }

    private BigDecimal placeLimitOrderSellAndWait(String figi, String brokerAccountId, BigDecimal lots, BigDecimal price) throws InterruptedException {

        LimitOrderRequestDto limitOrderRequest = createLimitOrderRequest(price, OperationType.SELL, lots);

        PlacedLimitOrderDto placedLimitOrder = brokerApi.placeLimitOrder(brokerAccountId, figi, limitOrderRequest);

        if (placedLimitOrder.getStatus() == OrderStatus.REJECTED || placedLimitOrder.getStatus() == OrderStatus.CANCELLED) {
            telegramNotificationService.sendNotificationTech("Для " + figi + " не удалось выставить заявку на продажу по цене " + price + "\n");
            return null;
        }
        telegramNotificationService.sendNotificationTech("Для " + figi + " выставлена заявка на продажу по цене " + price + "\n");

        for (int i = 0; i < 35; i++) {
            List<OrderDto> orders = brokerApi.getActiveOrders(brokerAccountId, figi);
            if (orders.isEmpty()) {

                return price;
            }
            if (!isTestMode) {
                Thread.sleep(DEFAULT_THREAD_SLEEP_VALUE);
            }
        }

        brokerApi.cancelOrder(brokerAccountId, figi, placedLimitOrder.getOrderId());
        telegramNotificationService.sendNotificationTech("Не удалось выполнить заявку");

        return null;
    }

    private BigDecimal placeLimitOrderBuyAndWait(String figi, String brokerAccountId, BigDecimal price, BigDecimal lots) throws InterruptedException {

        //boolean isPriceValid = validatePrice(price, figi);
        //if (!isPriceValid) {
        //  return null;
        //}

        LimitOrderRequestDto limitOrderRequest = createLimitOrderRequest(price, OperationType.BUY, lots);

        PlacedLimitOrderDto placedLimitOrder = brokerApi.placeLimitOrder(brokerAccountId, figi, limitOrderRequest);

        telegramNotificationService.sendNotificationTech("Для " + figi + " выставлена заявка на покупку по цене " + price + "\n");
        if (!isTestMode) {
            Thread.sleep(DEFAULT_THREAD_SLEEP_VALUE / 2);
        }

        for (int i = 0; i < 50; i++) {
            OrderDto orders = brokerApi.getActiveOrder(brokerAccountId, figi, placedLimitOrder.getOrderId());//brokerApi.getActiveOrders(brokerAccountId, figi);
            if (orders == null) {

                return price;
            }
            if (!isTestMode) {

                Thread.sleep(DEFAULT_THREAD_SLEEP_VALUE / 2);
            }
        }
        log.info("Заявку закрыть не получилось");
        brokerApi.cancelOrder(brokerAccountId, figi, placedLimitOrder.getOrderId());

        telegramNotificationService.sendNotificationTech("Не удалось исполнить заявку. Возвращаемся к выбору акций");
        return null;
    }

    public OrderDto getActiveOrder(String brokerAccountId, String figi) {

        return brokerApi.getActiveOrders(brokerAccountId, figi).stream()
                .findFirst().orElse(null);
    }

    public boolean hasProductInPortfolio(String brokerAccountId, String figi) {
        PortfolioPositionDto activePosition = brokerApi.getPortfolioPosition(figi, null, brokerAccountId);

        return activePosition != null;
    }

    private LimitOrderRequestDto createLimitOrderRequest(BigDecimal price, OperationType type, BigDecimal lots) {
        return LimitOrderRequestDto.builder()
                .lots(lots)
                .operation(type)
                .price(price)
                .build();
    }

}
