package com.alexproj.tradingsystem.service;

import ru.tinkoff.invest.openapi.model.rest.Currency;
import ru.tinkoff.invest.openapi.model.rest.Operation;
import ru.tinkoff.invest.openapi.model.rest.OperationTypeWithCommission;

import java.time.OffsetDateTime;
import java.util.List;

public interface AccountInfoService {
    List<Operation> getHistoryEntries(String brokerAccountid, OperationTypeWithCommission type,
                                      String figi, OffsetDateTime startDate, OffsetDateTime endDate, Currency currency);
}
