package com.alexproj.tradingsystem.service;

import java.util.List;

public interface DecisionMultipleInstrumentService extends DecisionService {

    String selectInstrumentToBuy(List<String> figis);
}
