package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.streaming.StreamingSubscriptionAction;
import ru.tinkoff.piapi.contract.v1.MarketDataRequest;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface StreamingInfoService {
    /**
     * Метод подписки на поток цен
     * В метод передается список объектов для работы с акцией (коином), необходимо в обработчике цен вызывать метод process и передавать туда цены
     * @param figis
     * @param brokerAccountId
     * @param processors
     * @throws InterruptedException
     */
    void subscribeMarketData(List<String> figis, String brokerAccountId, Map<String, TradingProcessor> processors) throws InterruptedException, IOException;
    MarketDataRequest createSubscribeCandlesRequest(List<String> figis, StreamingSubscriptionAction action);
}
