package com.alexproj.tradingsystem.service.tinkoff;

import com.alexproj.tradingsystem.service.AccountInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import ru.tinkoff.invest.openapi.OpenApi;
import ru.tinkoff.invest.openapi.model.rest.*;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;


//@ConditionalOnProperty(prefix = "bot", name = "brokerType", havingValue = "tinkoff")
//@Service
@RequiredArgsConstructor
public class AccountInfoServiceImpl implements AccountInfoService {

    private final OpenApi api;

    public List<Operation> getHistoryEntries(String brokerAccountId, OperationTypeWithCommission type,
                                             String figi, OffsetDateTime startDate, OffsetDateTime endDate, Currency currency) {
        return api.getOperationsContext().getOperations(startDate, endDate, figi, brokerAccountId).join()
                .getOperations()
                .stream()
                .filter(x -> x.getStatus().equals(OperationStatus.DONE))
                .filter(x -> type == null || x.getOperationType().equals(type))
                .filter(x -> InstrumentType.STOCK.equals(x.getInstrumentType()))
                .filter(x -> x.getCurrency().equals(currency))
                .filter(x -> figi == null || x.getFigi().equals(figi))
                .collect(Collectors.toList());

    }
}
