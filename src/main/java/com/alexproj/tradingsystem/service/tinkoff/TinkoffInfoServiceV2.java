package com.alexproj.tradingsystem.service.tinkoff;

import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.service.InfoService;
import com.google.protobuf.Timestamp;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import ru.tinkoff.piapi.contract.v1.*;

import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.alexproj.tradingsystem.util.MoneyUtils.createFromQuotation;

@Service
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "bot", name = "brokerType", havingValue = "tinkoff", matchIfMissing = true)
public class TinkoffInfoServiceV2 implements InfoService {

    private final static ZoneId ZONE_ID = ZonedDateTime.now().getZone();
    private final MarketDataServiceGrpc.MarketDataServiceBlockingStub marketDataServiceBlockingStub;

    @Override
    public List<CandleDto> getCandles(String figi, OffsetDateTime startDate, OffsetDateTime endDate, Duration duration) {
        List<CandleDto> candles = new ArrayList<>();

        Duration durationBetween = Duration.between(startDate, endDate);
        if (duration.toMinutes() > 0 && durationBetween.toDays() > 0) {
            for (int i = 0; i < durationBetween.toDays(); i++) {
                candles.addAll(getCandlesRequest(figi,
                        endDate.minusDays(durationBetween.toDays() - i),
                        endDate.minusDays(durationBetween.toDays() - i - 1),
                        duration));
            }
            return candles;
        }
        if (duration.toDays() == 0 && endDate.minusDays(7).isBefore(startDate)) {
            return getCandlesRequest(figi, startDate, endDate, duration);
        }
        if (startDate.plusDays(7).compareTo(endDate) >= 0) {
            return getCandlesRequest(figi, startDate, endDate, duration);
        }

        OffsetDateTime currentSearchDate = startDate;


        long diff = Duration.between(currentSearchDate, endDate).toDays();
        while (currentSearchDate.plusDays(diff).toLocalDate().compareTo(endDate.toLocalDate()) <= 0 && diff != 0) {

            long daysIncrement = diff > 7 ? 7L : diff;
            candles.addAll(getCandlesRequest(figi, currentSearchDate, currentSearchDate.plusDays(daysIncrement), duration));
            currentSearchDate = currentSearchDate.plusDays(daysIncrement);
            diff = Duration.between(currentSearchDate, endDate).toDays();
        }
        return candles;
    }

    private List<CandleDto> getCandlesRequest(@NonNull String figi, @NonNull OffsetDateTime startDate, @NonNull OffsetDateTime endDate, Duration duration) {
        Timestamp timestampFrom = Timestamp.newBuilder()
                .setSeconds(startDate.toInstant().getEpochSecond())
                .setNanos(startDate.toInstant().getNano()).build();
        Timestamp timestampTo = Timestamp.newBuilder()
                .setSeconds(endDate.toInstant().getEpochSecond())
                .setNanos(endDate.toInstant().getNano()).build();

        GetCandlesRequest request = GetCandlesRequest.newBuilder()
                .setFigi(figi)
                .setFrom(timestampFrom)
                .setTo(timestampTo)
                .setInterval(mapTo(duration))
                .build();

        GetCandlesResponse response = marketDataServiceBlockingStub.getCandles(request);
        return response.getCandlesList().stream().map(candle -> mapTo(candle, figi, duration)).collect(Collectors.toList());
    }

    private CandleInterval mapTo(Duration duration) {
        if (duration.equals(Duration.ofMinutes(1))) {
            return CandleInterval.CANDLE_INTERVAL_1_MIN;
        }
        if (duration.equals(Duration.ofMinutes(5))) {
            return CandleInterval.CANDLE_INTERVAL_5_MIN;
        }
        if (duration.equals(Duration.ofMinutes(15))) {
            return CandleInterval.CANDLE_INTERVAL_15_MIN;
        }
        if (duration.equals(Duration.ofHours(1))) {
            return CandleInterval.CANDLE_INTERVAL_HOUR;
        }
        if (duration.equals(Duration.ofDays(1))) {
            return CandleInterval.CANDLE_INTERVAL_DAY;
        }
        return CandleInterval.CANDLE_INTERVAL_DAY;
    }

    private CandleDto mapTo(HistoricCandle candle, String figi, Duration duration) {
        OffsetDateTime time = OffsetDateTime.ofInstant(
                Instant.ofEpochSecond(candle.getTime().getSeconds(), candle.getTime().getNanos()), ZONE_ID);
        return CandleDto.builder()
                .figi(figi)
                .low(createFromQuotation(candle.getLow()))
                .high(createFromQuotation(candle.getHigh()))
                .open(createFromQuotation(candle.getOpen()))
                .close(createFromQuotation(candle.getClose()))
                .volume(candle.getVolume())
                .time(time)
                .interval(duration)
                .build();
    }
}
