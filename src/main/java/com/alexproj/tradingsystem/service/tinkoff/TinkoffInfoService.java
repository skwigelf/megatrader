package com.alexproj.tradingsystem.service.tinkoff;

import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.mapper.CandleMapper;
import com.alexproj.tradingsystem.service.InfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import ru.tinkoff.invest.openapi.OpenApi;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

//@Service
@RequiredArgsConstructor
//@ConditionalOnProperty(prefix = "bot", name = "brokerType", havingValue = "tinkoff", matchIfMissing = true)
public class TinkoffInfoService implements InfoService {

    private final OpenApi api;

    public List<CandleDto> getCandles(String figi, OffsetDateTime startDate, OffsetDateTime endDate, Duration duration) {
        List<CandleDto> candles = new ArrayList<>();

        Duration durationBetween = Duration.between(startDate, endDate);
        if (duration.toMinutes() > 0 && durationBetween.toDays() > 0) {
            for (int i = 0; i < durationBetween.toDays(); i++) {
                candles.addAll(getCandlesRequest(figi,
                        endDate.minusDays(durationBetween.toDays() - i),
                        endDate.minusDays(durationBetween.toDays() - i - 1),
                        duration));
            }
            return candles;
        }
        if (duration.toDays() == 0 && endDate.minusDays(7).isBefore(startDate)) {
            return getCandlesRequest(figi, startDate, endDate, duration);
        }
        if (startDate.plusDays(7).compareTo(endDate) >= 0) {
            return getCandlesRequest(figi, startDate, endDate, duration);
        }

        OffsetDateTime currentSearchDate = startDate;


        long diff = Duration.between(currentSearchDate, endDate).toDays();
        while (currentSearchDate.plusDays(diff).toLocalDate().compareTo(endDate.toLocalDate()) <= 0 && diff != 0) {

            long daysIncrement = diff > 7 ? 7L : diff;
            candles.addAll(getCandlesRequest(figi, currentSearchDate, currentSearchDate.plusDays(daysIncrement), duration));
            currentSearchDate = currentSearchDate.plusDays(daysIncrement);
            diff = Duration.between(currentSearchDate, endDate).toDays();
        }
        return candles;
    }

    private List<CandleDto> getCandlesRequest(String figi, OffsetDateTime start, OffsetDateTime end, Duration duration) {
        try {
            return api.getMarketContext().getMarketCandles(
                            figi,
                            start,
                            end,
                            CandleMapper.getCandleResolutionFromDuration(duration)
                    ).join().orElseThrow()
                    .getCandles()
                    .stream()
                    .map(CandleMapper::map)
                    .collect(toList());
        } catch (Exception e) {
            return api.getMarketContext().getMarketCandles(
                            figi,
                            start,
                            end,
                            CandleMapper.getCandleResolutionFromDuration(duration)
                    ).join().orElseThrow()
                    .getCandles()
                    .stream()
                    .map(CandleMapper::map)
                    .collect(toList());
        }
    }

}
