package com.alexproj.tradingsystem.service.tinkoff;

import com.alexproj.tradingsystem.dto.OperationType;
import com.alexproj.tradingsystem.dto.OperationTypeWithCommission;
import com.alexproj.tradingsystem.dto.OrderStatus;
import com.alexproj.tradingsystem.dto.*;
import com.alexproj.tradingsystem.exceptions.WrongMappingException;
import ru.tinkoff.invest.openapi.model.rest.Currency;
import ru.tinkoff.invest.openapi.model.rest.Operation;
import ru.tinkoff.invest.openapi.model.rest.Order;
import ru.tinkoff.invest.openapi.model.rest.PortfolioPosition;
import ru.tinkoff.invest.openapi.model.rest.*;
import ru.tinkoff.piapi.contract.v1.*;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.alexproj.tradingsystem.util.MoneyUtils.createFromQuotation;
import static ru.tinkoff.piapi.contract.v1.OrderExecutionReportStatus.*;


//@Component
//@RequiredArgsConstructor
//@ConditionalOnProperty(prefix = "bot", name = "brokerType", havingValue = "tinkoff")
public class TinkoffMapper {

    public static Currency mapTo(CurrencyType currencyType) {
        if (currencyType == CurrencyType.RUB) {
            return Currency.RUB;
        }
        if (currencyType == CurrencyType.USD) {
            return Currency.USD;
        }
        if (currencyType == CurrencyType.EUR) {
            return Currency.EUR;
        }
        throw new WrongMappingException();
    }

    public static CurrencyType mapTo(String currencyType) {
        if (currencyType.equalsIgnoreCase(CurrencyType.RUB.name())) {
            return CurrencyType.RUB;
        }
        if (currencyType.equalsIgnoreCase(CurrencyType.USD.name())) {
            return CurrencyType.USD;
        }
        if (currencyType.equalsIgnoreCase(CurrencyType.EUR.name())) {
            return CurrencyType.EUR;
        }
        throw new WrongMappingException();
    }

    public InstrumentType mapTo(MarketInstrumentType type) {
        return InstrumentType.valueOf(type.getValue());
    }

    public static MarketInstrumentType mapTo(InstrumentType type) {
        return MarketInstrumentType.valueOf(type.getValue());
    }

    public MarketInstrumentDto mapTo(MarketInstrument marketInstrument) {
        return MarketInstrumentDto.builder()
                .currency(marketInstrument.getCurrency())
                .figi(marketInstrument.getFigi())
                .isin(marketInstrument.getIsin())
                .minPriceIncrement(marketInstrument.getMinPriceIncrement())
                .minQuantity(marketInstrument.getMinQuantity())
                .lot(marketInstrument.getLot())
                .name(marketInstrument.getName())
                .ticker(marketInstrument.getTicker())
                .type(mapTo(marketInstrument.getType()))
                .build();
    }

    public static PortfolioPositionDto mapTo(PortfolioPosition portfolioPosition) {
        return PortfolioPositionDto.builder()
                .figi(portfolioPosition.getFigi())
                .lots(BigDecimal.valueOf(portfolioPosition.getLots()))
                .build();
    }

    public static PortfolioPositionDto mapTo(PositionsSecurities positionsSecurities, SearchMarketInstrumentDto instrument) {
        return PortfolioPositionDto.builder()
                .figi(positionsSecurities.getFigi())
                .lots(BigDecimal.valueOf(positionsSecurities.getBalance())
                        .divide(instrument != null ? instrument.getLot() : BigDecimal.ONE, RoundingMode.HALF_UP))
                .build();
    }

    public static OrderDto mapTo(Order order) {
        return OrderDto.builder()
                .figi(order.getFigi())
                .build();
    }

    public static OrderDto mapTo(OrderState order) {
        return OrderDto.builder()
                .figi(order.getFigi())
                .build();
    }


    public static SearchMarketInstrumentDto mapTo(SearchMarketInstrument searchMarketInstrument) {
        return SearchMarketInstrumentDto.builder()
                .figi(searchMarketInstrument.getFigi())
                .name(searchMarketInstrument.getName())
                .build();
    }

    public static SearchMarketInstrumentDto mapTo(InstrumentResponse response) {
        return SearchMarketInstrumentDto.builder()
                .figi(response.getInstrument().getFigi())
                .name(response.getInstrument().getName())
                .lot(BigDecimal.valueOf(response.getInstrument().getLot()))
                .minPriceIncrement(createFromQuotation(response.getInstrument().getMinPriceIncrement()))
                .dlong(createFromQuotation(response.getInstrument().getDlong()))
                .build();
    }

    public static OperationDto mapTo(Operation operation) {
        return OperationDto.builder()
                .figi(operation.getFigi())
                .price(operation.getPrice())
                .date(operation.getDate())
                .operationType(mapTo(operation.getOperationType()))
                .build();
    }

    public static PlacedLimitOrderDto mapTo(PlacedLimitOrder placedLimitOrder) {
        return PlacedLimitOrderDto.builder()
                .orderId(placedLimitOrder.getOrderId())
                .status(mapTo(placedLimitOrder.getStatus()))
                .build();
    }

    public static PlacedLimitOrderDto mapTo(PostOrderResponse placedLimitOrder) {
        return PlacedLimitOrderDto.builder()
                .orderId(placedLimitOrder.getOrderId())
                .status(mapTo(placedLimitOrder.getExecutionReportStatus()))
                .build();
    }

    public static LimitOrderRequestDto mapTo(LimitOrderRequest limitOrderRequest) {
        return LimitOrderRequestDto.builder()
                .lots(BigDecimal.valueOf(limitOrderRequest.getLots()))
                .price(limitOrderRequest.getPrice())
                .operation(mapTo(limitOrderRequest.getOperation()))
                .build();
    }

    public static LimitOrderRequest mapFrom(LimitOrderRequestDto limitOrderRequest) {
        LimitOrderRequest limitOrder = new LimitOrderRequest();
        limitOrder.setLots(limitOrderRequest.getLots().toBigInteger().intValue());
        limitOrder.setPrice(limitOrderRequest.getPrice());
        limitOrder.setOperation(mapFrom(limitOrderRequest.getOperation()));

        return limitOrder;
    }

    private static OrderStatus mapTo(ru.tinkoff.invest.openapi.model.rest.OrderStatus orderStatus) {
        return OrderStatus.valueOf(orderStatus.name());
    }

    private static OperationTypeWithCommission mapTo(ru.tinkoff.invest.openapi.model.rest.OperationTypeWithCommission type) {
        return OperationTypeWithCommission.valueOf(type.name());
    }

    private static OperationType mapTo(ru.tinkoff.invest.openapi.model.rest.OperationType type) {
        return OperationType.valueOf(type.name());
    }

    private static ru.tinkoff.invest.openapi.model.rest.OperationType mapFrom(OperationType type) {
        return ru.tinkoff.invest.openapi.model.rest.OperationType.valueOf(type.name());
    }

    private static OrderStatus mapTo(OrderExecutionReportStatus status) {
        if (status == EXECUTION_REPORT_STATUS_UNSPECIFIED) {
            throw new RuntimeException("WRONG STATUS OF ORDER!!!");
        }
        if (status == EXECUTION_REPORT_STATUS_FILL) {
            return OrderStatus.FILL;
        }
        if (status == EXECUTION_REPORT_STATUS_REJECTED) {
            return OrderStatus.REJECTED;
        }
        if (status == EXECUTION_REPORT_STATUS_CANCELLED) {
            return OrderStatus.CANCELLED;
        }
        if (status == EXECUTION_REPORT_STATUS_NEW) {
            return OrderStatus.NEW;
        }
        if (status == EXECUTION_REPORT_STATUS_PARTIALLYFILL) {
            return OrderStatus.PARTIALLYFILL;
        }
        throw new RuntimeException("WRONG STATUS OF ORDER!!!");
    }
}
