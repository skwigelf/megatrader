package com.alexproj.tradingsystem.service.tinkoff;

import com.alexproj.tradingsystem.dto.*;
import com.alexproj.tradingsystem.dto.OperationType;
import com.alexproj.tradingsystem.notifications.TelegramNotificationService;
import com.alexproj.tradingsystem.service.BrokerApi;
import com.alexproj.tradingsystem.util.MoneyUtils;
import com.google.protobuf.Timestamp;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;
import ru.tinkoff.invest.openapi.OpenApi;
import ru.tinkoff.piapi.contract.v1.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.*;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.alexproj.tradingsystem.util.MoneyUtils.createFromBigDecimal;

@Component
@RequiredArgsConstructor
@Slf4j
@ConditionalOnExpression("'${bot.brokerType}'.equals('tinkoff') and not ${bot.mock:true}")
public class TinkoffBrokerApi implements BrokerApi {

    private final OpenApi api;

    private final TelegramNotificationService notificationService;
    private final InstrumentsServiceGrpc.InstrumentsServiceBlockingStub instrumentsServiceBlockingStub;

    private final OperationsServiceGrpc.OperationsServiceBlockingStub operationsServiceBlockingStub;

    private final UsersServiceGrpc.UsersServiceBlockingStub usersServiceBlockingStub;

    private final OrdersServiceGrpc.OrdersServiceBlockingStub ordersServiceBlockingStub;

    private final MarketDataServiceGrpc.MarketDataServiceBlockingStub marketDataServiceBlockingStub;

    @Override
    public BigDecimal getCurrencyBalance(String brokerAccountId, CurrencyType currencyType) {
        WithdrawLimitsRequest request = WithdrawLimitsRequest
                .newBuilder()
                .setAccountId(brokerAccountId)
                .build();

        WithdrawLimitsResponse response = operationsServiceBlockingStub.getWithdrawLimits(request);

        List<MoneyValue> moneys = response.getMoneyList();

        return moneys.stream().filter(moneyValue -> TinkoffMapper.mapTo(moneyValue.getCurrency()) == currencyType)
                .findFirst()
                .map(moneyValue -> MoneyUtils.createFromUnitsAndNanos(moneyValue.getUnits(), moneyValue.getNano()))
                .orElse(null);
    }

    @Override
    public List<String> getFigisNames(List<String> figis) {

        return figis.stream().map(figi -> {
            InstrumentRequest request = InstrumentRequest.newBuilder()
                    .setId(figi)
                    .setIdType(InstrumentIdType.INSTRUMENT_ID_TYPE_FIGI)
                    .build();
            return instrumentsServiceBlockingStub.getInstrumentBy(request).getInstrument().getName();
        }).collect(Collectors.toList());
    }

    @Override
    public PortfolioPositionDto getPortfolioPosition(String figi, SearchMarketInstrumentDto instrument, String brokerAccountId) {
        PositionsRequest request = PositionsRequest.newBuilder()
                .setAccountId(brokerAccountId)
                .build();

        PositionsResponse response = operationsServiceBlockingStub.getPositions(request);

        return response.getSecuritiesList().stream()
                .map(position -> TinkoffMapper.mapTo(position, instrument))
                .filter(position -> position.getFigi().equals(figi))
                .findFirst().orElse(null);
    }

    @Override
    public List<OrderDto> getActiveOrders(String brokerAccountId, String figi) {
        GetOrdersRequest request = GetOrdersRequest.newBuilder()
                .setAccountId(brokerAccountId)
                .build();
        GetOrdersResponse response = ordersServiceBlockingStub.getOrders(request);

        return response.getOrdersList()
                .stream()
                .map(TinkoffMapper::mapTo)
                .collect(Collectors.toList());
    }

    @Override
    public OrderDto getActiveOrder(String brokerAccountId, String figi, String orderId) {
        GetOrdersRequest request = GetOrdersRequest.newBuilder()
                .setAccountId(brokerAccountId)
                .build();
        GetOrdersResponse response = ordersServiceBlockingStub.getOrders(request);

        return response.getOrdersList()
                .stream()
                .map(TinkoffMapper::mapTo)
                .findFirst().orElse(null);
    }

    @Override
    public SearchMarketInstrumentDto searchByFigi(String figi) {
        InstrumentRequest request = InstrumentRequest.newBuilder()
                .setId(figi)
                .setIdType(InstrumentIdType.INSTRUMENT_ID_TYPE_FIGI)
                .build();

        InstrumentResponse response = instrumentsServiceBlockingStub.getInstrumentBy(request);

        log.info(response.getInstrument().getFigi());
        log.info(String.valueOf(response.getInstrument().getDlongMin()));
        log.info(String.valueOf(response.getInstrument().getDlong()));
        log.info(String.valueOf(response.getInstrument().getKlong()));
        log.info(String.valueOf(response.getInstrument().getBuyAvailableFlag()));
        log.info(response.getInstrument().getCurrency());

        if (response == null) {
            throw new RuntimeException("Не удалось найти акцию по идентификатору " + figi);
        }

        return TinkoffMapper.mapTo(response);
    }

    @Override
    public OperationDto getLastPositionLastDay(String figi, String brokerAccountId, OperationTypeWithCommission type) {
        try {
            return api.getOperationsContext()
                    .getOperations(OffsetDateTime.now().minusDays(5), OffsetDateTime.now(), figi, brokerAccountId)
                    .join().getOperations().stream()
                    .map(TinkoffMapper::mapTo)
                    .filter(x -> x.getOperationType() == type)
                    .max(Comparator.comparing(OperationDto::getDate)).orElse(null);
        } catch (Exception e) {
            return api.getOperationsContext()
                    .getOperations(OffsetDateTime.now().minusDays(5), OffsetDateTime.now(), figi, brokerAccountId)
                    .join().getOperations().stream()
                    .map(TinkoffMapper::mapTo)
                    .filter(x -> x.getOperationType() == type)
                    .max(Comparator.comparing(OperationDto::getDate)).orElse(null);
        }
    }

    @Override
    public void cancelOrder(String brokerAccountId, String figi, String id) {
        CancelOrderRequest request = CancelOrderRequest.newBuilder().setAccountId(brokerAccountId).setOrderId(id).build();

        ordersServiceBlockingStub.cancelOrder(request);
    }

    @Override
    public PlacedLimitOrderDto placeLimitOrder(String brokerAccountId, String figi, LimitOrderRequestDto limitOrderRequest) {
        PostOrderRequest request = PostOrderRequest.newBuilder()
                .setAccountId(brokerAccountId)
                .setFigi(figi)
                .setQuantity(limitOrderRequest.getLots().longValue())
                .setDirection(map(limitOrderRequest.getOperation()))
                .setPrice(createFromBigDecimal(limitOrderRequest.getPrice()))
                .setOrderId(String.valueOf(System.currentTimeMillis()))
                .setOrderType(OrderType.ORDER_TYPE_LIMIT)
                .build();
        log.info(request.toString());
        return TinkoffMapper.mapTo(ordersServiceBlockingStub.postOrder(request));
    }

    private OrderDirection map(OperationType operationType) {
        if (operationType == OperationType.BUY) {
            return OrderDirection.ORDER_DIRECTION_BUY;
        }
        if (operationType == OperationType.SELL) {
            return OrderDirection.ORDER_DIRECTION_SELL;
        }
        throw new RuntimeException("Unknown order type!!!" + operationType.name());
    }

    @Override
    public void close() throws IOException {
        api.close();
    }

    @Override
    public String getBrokerAccountId() {
        GetAccountsRequest request = GetAccountsRequest.newBuilder().build();
        return usersServiceBlockingStub.getAccounts(request).getAccountsList().stream()
                .filter(acc -> acc.getName().equals("Брокерский счёт")).findFirst().orElseThrow().getId();
    }

    public Map<String, Boolean> hasNoDividends(List<String> figis) {
        LocalDateTime timeFrom = LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.of(0, 0, 0, 0));
        LocalDateTime timeTo = LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.of(0, 0, 0, 0));

        Timestamp timestampFrom = Timestamp.newBuilder()
                .setSeconds(timeFrom.toInstant(ZoneOffset.UTC).getEpochSecond())
                .setNanos(timeFrom.toInstant(ZoneOffset.UTC).getNano()).build();

        Timestamp timestampTo = Timestamp.newBuilder()
                .setSeconds(timeTo.toInstant(ZoneOffset.UTC).getEpochSecond())
                .setNanos(timeTo.toInstant(ZoneOffset.UTC).getNano()).build();

        return figis.stream().collect(Collectors.toMap(figi -> figi, figi -> {
            GetDividendsRequest getDividendsRequest = GetDividendsRequest
                    .newBuilder()
                    .setFigi(figis.get(0))
                    .setFrom(timestampFrom)
                    .setTo(timestampTo)
                    .setFigi(figi)
                    .build();

            GetDividendsResponse response = instrumentsServiceBlockingStub.getDividends(getDividendsRequest);

            return response.getDividendsList().isEmpty();
        }));
    }
}
