package com.alexproj.tradingsystem.service.tinkoff;

import com.alexproj.tradingsystem.dto.streaming.StreamingSubscriptionAction;
import com.alexproj.tradingsystem.exceptions.StreamingException;
import com.alexproj.tradingsystem.mapper.TinkoffStreamingMapper;
import com.alexproj.tradingsystem.notifications.ServiceNotification;
import com.alexproj.tradingsystem.service.StreamingInfoService;
import com.alexproj.tradingsystem.service.TradingProcessor;
import com.alexproj.tradingsystem.service.dto.OrderBookDto;
import com.alexproj.tradingsystem.service.dto.PriceDto;
import com.alexproj.tradingsystem.service.dto.TradeDto;
import com.alexproj.tradingsystem.util.MoneyUtils;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;
import ru.tinkoff.piapi.contract.v1.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

//@Service
@Slf4j
@Service
@ConditionalOnExpression("'${bot.brokerType}'.equals('tinkoff') and not ${bot.mock:true}")
@RequiredArgsConstructor
public class StreamingInfoServiceTinkoff implements StreamingInfoService {

    private static final int POOL_THREADS = 6;

    private final AtomicBoolean isSubscriptionPricesActive = new AtomicBoolean(false);

    private final ExecutorService pool = Executors.newFixedThreadPool(POOL_THREADS);

    private final MarketDataStreamServiceGrpc.MarketDataStreamServiceStub marketDataStreamServiceStub;

    private final ServiceNotification telegramNotificationService;

    private final TinkoffStreamingMapper mapper;

    public boolean isSubscriptionPricesActive() {
        return isSubscriptionPricesActive.get();
    }

    public void setSubscriptionActive(boolean value) {
        isSubscriptionPricesActive.set(value);
    }

    public void subscribeMarketData(
            List<String> figis, String brokerAccountId, Map<String, TradingProcessor> processors) throws InterruptedException {

        StreamObserver<MarketDataRequest> streamRequest = createMarketDataRequestPriceObserver(
                (price, figi) -> processors.get(figi)
                        .process(new PriceDto(price, null, null, null),
                                brokerAccountId)

        );

        StreamObserver<MarketDataRequest> tradesRequest = createMarketDataRequestTradeObserver(
                (trade, figi) -> processors.get(figi)
                        .handleTrade(trade)
        );

        StreamObserver<MarketDataRequest> ordersRequest = createMarketDataRequestOrderBookObserver(
                (orderBook, figi) -> processors.get(figi)
                        .handleOrderBook(orderBook)
        );

        setSubscriptionActive(true);
        streamRequest.onNext(createSubscribeCandlesRequest(figis, StreamingSubscriptionAction.SUBSCRIBE));
        tradesRequest.onNext(createSubscribeTradesRequest(figis, StreamingSubscriptionAction.SUBSCRIBE));
        ordersRequest.onNext(createSubscribeOrderBookRequest(figis, StreamingSubscriptionAction.SUBSCRIBE));

        while (true) {
            if (!isSubscriptionPricesActive()) {
                throw new StreamingException();
            }
            Thread.sleep(2000);
        }

    }

    private StreamObserver<MarketDataRequest> createMarketDataRequestPriceObserver(
            BiConsumer<BigDecimal, String> handlePrice) {
        return marketDataStreamServiceStub.marketDataStream(new StreamObserver<>() {
            @Override
            public void onNext(MarketDataResponse value) {
                if (!value.getCandle().getFigi().equals("")) {
                    CompletableFuture.runAsync(() -> {
                        Quotation close = value.getCandle().getClose();
                        String figi = value.getCandle().getFigi();

                        handlePrice.accept(MoneyUtils.createFromQuotation(close), figi);
                    }, pool);
                }


            }

            @Override
            public void onError(Throwable t) {
                t.printStackTrace();
                telegramNotificationService.sendNotificationTech("Возникла ошибка во время получения цен. Требуется перезапустить бота");
                log.info("ОШИБКА");
                isSubscriptionPricesActive.set(false);
            }

            @Override
            public void onCompleted() {
                log.info("ГОТОВО");
            }
        });
    }

    private StreamObserver<MarketDataRequest> createMarketDataRequestTradeObserver(
            BiConsumer<TradeDto, String> handleTrade) {
        return marketDataStreamServiceStub.marketDataStream(new StreamObserver<>() {
            @Override
            public void onNext(MarketDataResponse value) {
                if (!value.getTrade().getFigi().equals("")) {
                    CompletableFuture.runAsync(() -> {
                        String figi = value.getTrade().getFigi();

                        handleTrade.accept(mapper.map(value.getTrade()), figi);
                    }, pool);
                }

            }

            @Override
            public void onError(Throwable t) {
                t.printStackTrace();
                telegramNotificationService.sendNotificationTech("Возникла ошибка во время получения цен. Требуется перезапустить бота");
                log.info("ОШИБКА");
                isSubscriptionPricesActive.set(false);
            }

            @Override
            public void onCompleted() {
                log.info("ГОТОВО");
            }
        });
    }

    private StreamObserver<MarketDataRequest> createMarketDataRequestOrderBookObserver(
            BiConsumer<OrderBookDto, String> handleOrderBook) {
        return marketDataStreamServiceStub.marketDataStream(new StreamObserver<>() {
            @Override
            public void onNext(MarketDataResponse value) {
                if (!value.getOrderbook().getFigi().equals("")) {
                    CompletableFuture.runAsync(() -> {
                        String figi = value.getTrade().getFigi();

                        handleOrderBook.accept(mapper.map(value.getOrderbook()), figi);
                    }, pool);
                }

            }

            @Override
            public void onError(Throwable t) {
                t.printStackTrace();
                telegramNotificationService.sendNotificationTech("Возникла ошибка во время получения цен. Требуется перезапустить бота");
                log.info("ОШИБКА");
                isSubscriptionPricesActive.set(false);
            }

            @Override
            public void onCompleted() {
                log.info("ГОТОВО");
            }
        });
    }

    public MarketDataRequest createSubscribeCandlesRequest(List<String> figis, StreamingSubscriptionAction action) {

        SubscriptionAction subscriptionAction = mapper.map(action);

        List<CandleInstrument> candles = figis.stream().map(
                figi -> CandleInstrument.newBuilder()
                        .setFigi(figi)
                        .setInterval(SubscriptionInterval.SUBSCRIPTION_INTERVAL_ONE_MINUTE)
                        .build()
        ).collect(Collectors.toList());

        return MarketDataRequest.newBuilder()
                .setSubscribeCandlesRequest(SubscribeCandlesRequest.newBuilder()
                        .setSubscriptionAction(subscriptionAction)
                        .addAllInstruments(candles)
                        .build())
                .build();
    }

    public MarketDataRequest createSubscribeOrderBookRequest(List<String> figis, StreamingSubscriptionAction action) {

        SubscriptionAction subscriptionAction = mapper.map(action);

        List<OrderBookInstrument> instruments = figis.stream().map(
                figi -> OrderBookInstrument.newBuilder().setFigi(figi).build()
        ).collect(Collectors.toList());

        return MarketDataRequest.newBuilder()
                .setSubscribeOrderBookRequest(SubscribeOrderBookRequest.newBuilder()
                        .setSubscriptionAction(subscriptionAction)
                        .addAllInstruments(instruments)
                        .build())
                .build();
    }

    public MarketDataRequest createSubscribeTradesRequest(List<String> figis, StreamingSubscriptionAction action) {

        SubscriptionAction subscriptionAction = mapper.map(action);

        List<TradeInstrument> instruments = figis.stream().map(
                figi -> TradeInstrument.newBuilder().setFigi(figi).build()
        ).collect(Collectors.toList());

        return MarketDataRequest.newBuilder()
                .setSubscribeTradesRequest(SubscribeTradesRequest.newBuilder()
                        .setSubscriptionAction(subscriptionAction)
                        .addAllInstruments(instruments)
                        .build())
                .build();
    }
}
