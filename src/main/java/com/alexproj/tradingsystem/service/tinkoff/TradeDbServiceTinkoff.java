package com.alexproj.tradingsystem.service.tinkoff;

import com.alexproj.tradingsystem.model.Trade;
import com.alexproj.tradingsystem.repo.TradeRepository;
import com.alexproj.tradingsystem.service.TradeDbService;
import com.alexproj.tradingsystem.service.dto.TradeDto;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
@ConditionalOnProperty(prefix = "bot", name = "brokerType", havingValue = "tinkoff", matchIfMissing = true)
public class TradeDbServiceTinkoff implements TradeDbService {

    private final TradeRepository tradeRepository;

    public Trade save(String figi, TradeDto tradeDto) {
        Trade trade = new Trade();
        trade.setFigi(figi);

        trade.setDirection(tradeDto.getDirection());
        trade.setPrice(tradeDto.getPrice());
        trade.setQuantity(tradeDto.getQuantity());
        trade.setTime(tradeDto.getTime());

        return tradeRepository.save(trade);
    }

    public List<Trade> findAllByFigi(String figi) {
        return tradeRepository.findAllByFigi(figi);
    }
}
