package com.alexproj.tradingsystem.service.tinkoff.decisionrules;

import com.alexproj.tradingsystem.service.dto.IndicatorDto;
import com.alexproj.tradingsystem.service.dto.TradeDirection;
import com.alexproj.tradingsystem.service.dto.TradeDto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

import static java.math.BigDecimal.valueOf;

public class DefaultTradesBuyRule implements Function<IndicatorDto, Boolean> {
    private static final BigDecimal BOLLINGER_DOWN_LIMIT = valueOf(0.0015);

    @Override
    public Boolean apply(IndicatorDto indicator) {
        BigDecimal bollingerDown = indicator.getBollingerDown();
        BigDecimal bollingerDownPercentage = bollingerDown.subtract(indicator.getPrice()).divide(bollingerDown, 5, RoundingMode.HALF_UP);

        long buyQuantity = indicator.getTrades().stream()
                .filter(trade -> trade.getDirection()== TradeDirection.TRADE_DIRECTION_BUY)
                .map(TradeDto::getQuantity)
                .reduce(Long::sum).orElse(0L);

        long sellQuantity = indicator.getTrades().stream()
                .filter(trade -> trade.getDirection()== TradeDirection.TRADE_DIRECTION_BUY)
                .map(TradeDto::getQuantity)
                .reduce(Long::sum).orElse(0L);

        boolean isTradesBuy = ((double) buyQuantity / (double) sellQuantity) > 1 && indicator.getTrades().size() > 35;

        return isTradesBuy && bollingerDownPercentage.compareTo(BOLLINGER_DOWN_LIMIT) > 0;
    }
}