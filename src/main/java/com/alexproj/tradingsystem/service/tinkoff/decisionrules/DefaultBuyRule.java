package com.alexproj.tradingsystem.service.tinkoff.decisionrules;

import com.alexproj.tradingsystem.service.dto.IndicatorDto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

import static java.math.BigDecimal.valueOf;

public class DefaultBuyRule implements Function<IndicatorDto, Boolean> {
    private static final BigDecimal BOLLINGER_DOWN_LIMIT = valueOf(0.0045);

    @Override
    public Boolean apply(IndicatorDto indicator) {
        BigDecimal bollingerDown = indicator.getBollingerDown();
        BigDecimal bollingerDownPercentage = bollingerDown.subtract(indicator.getPrice()).divide(bollingerDown, 5, RoundingMode.HALF_UP);

        return bollingerDownPercentage.compareTo(BOLLINGER_DOWN_LIMIT) > 0;
    }
}
