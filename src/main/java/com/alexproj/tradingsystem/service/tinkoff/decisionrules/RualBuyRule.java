package com.alexproj.tradingsystem.service.tinkoff.decisionrules;

import com.alexproj.tradingsystem.service.dto.IndicatorDto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.util.function.Function;

import static java.math.BigDecimal.valueOf;

public class RualBuyRule implements Function<IndicatorDto, Boolean> {
    private static final BigDecimal BOLLINGER_DOWN_LIMIT_MORNING = valueOf(0.0048);
    private static final BigDecimal BOLLINGER_DOWN_LIMIT = valueOf(0.006);
    private static final BigDecimal RSI_LIMIT = BigDecimal.valueOf(60);

    @Override
    public Boolean apply(IndicatorDto indicator) {
        //boolean lowRsi = indicator.getRsi().compareTo(RSI_LIMIT) < 0;

        BigDecimal bollingerDownLimit = BOLLINGER_DOWN_LIMIT;
        if (OffsetTime.now().isBefore(OffsetTime.of(10, 30, 0, 0, ZoneOffset.of("+03:00")))) {
            bollingerDownLimit = BOLLINGER_DOWN_LIMIT_MORNING;
        }
        BigDecimal bollingerDown = indicator.getBollingerDown();
        BigDecimal bollingerDownPercentage = bollingerDown.subtract(indicator.getPrice()).divide(bollingerDown, 5, RoundingMode.HALF_UP);
        boolean bollingerDownCondition = bollingerDownPercentage.compareTo(bollingerDownLimit) > 0;

        return bollingerDownCondition;
    }
}
