package com.alexproj.tradingsystem.service;

public interface BotExecutor {
    void startBot(boolean debugMode);
}
