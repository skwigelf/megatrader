package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.dto.SearchMarketInstrumentDto;
import com.alexproj.tradingsystem.service.tinkoff.TinkoffInfoServiceV2;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
@ConditionalOnExpression("not ${bot.mock:true}")
@RequiredArgsConstructor
public class CacheServiceImpl implements CacheService {
    private static final int PORTFOLIO_TIMES_EXECUTED = 10;

    private final Map<String, CandlesWithTime> cachedCandles = new ConcurrentHashMap<>();
    private final Map<String, SearchMarketInstrumentDto> cachedInstruments = new ConcurrentHashMap<>();
    private final Map<String, Boolean> hasStockInPortfolioByFigis = new ConcurrentHashMap<>();
    private Map<String, Integer> hasStockInPortfolioTimesExecutedMap = new ConcurrentHashMap<>();

    private String brokerAccountId;

    private final TinkoffInfoServiceV2 tinkoffInfoServiceV2;

    private final static Duration CANDLES_PERIOD = Duration.ofMinutes(5);

    private final BrokerApi brokerApi;

    public List<CandleDto> getCandles(String figi) {
        LocalDateTime now = LocalDateTime.now();
        CandlesWithTime cached = cachedCandles.get(figi);

        if (cached == null || cached.getCandles().size() < 25 || cached.getTime().plusMinutes(1).isBefore(now)) {
            LocalDateTime nowRoundUppedToMinutes = roundUpToMinutes(now);

            OffsetDateTime offsetNow = OffsetDateTime.now();
            boolean isMondayAndMinutesPeriod = now.getDayOfWeek() == DayOfWeek.MONDAY && CANDLES_PERIOD.toMinutes() > 0;
            cached = new CandlesWithTime(tinkoffInfoServiceV2.getCandles(figi,
                    isMondayAndMinutesPeriod ? offsetNow.minusDays(4) : offsetNow.minusDays(2),
                    offsetNow,
                    CANDLES_PERIOD), nowRoundUppedToMinutes);
            cachedCandles.put(figi, cached);
        }
        return cached.getCandles();
    }

    public SearchMarketInstrumentDto getMarketInstrument(String figi) {
        SearchMarketInstrumentDto marketInstrument = cachedInstruments.get(figi);
        if (marketInstrument == null) {
            marketInstrument = brokerApi.searchByFigi(figi);
            cachedInstruments.put(figi, marketInstrument);
        }
        return marketInstrument;

    }

    public boolean hasStockInPortfolio(String figi) {

        if (brokerAccountId == null) {
            brokerAccountId = brokerApi.getBrokerAccountId();
        }

        Boolean hasStockInPortfolio = hasStockInPortfolioByFigis.get(figi);
        Integer hasStockInPortfolioTimesExecuted = hasStockInPortfolioTimesExecutedMap.putIfAbsent(figi, 0);
        if (hasStockInPortfolioTimesExecuted == null) {
            hasStockInPortfolioTimesExecuted = 0;
        }

        if (hasStockInPortfolio == null || hasStockInPortfolioTimesExecuted == PORTFOLIO_TIMES_EXECUTED) {
            hasStockInPortfolio = brokerApi.getPortfolioPosition(figi, null, brokerAccountId) != null;
            hasStockInPortfolioByFigis.put(figi, hasStockInPortfolio);
            hasStockInPortfolioTimesExecutedMap.put(figi, 0);
        } else {
            hasStockInPortfolioTimesExecutedMap.put(figi, ++hasStockInPortfolioTimesExecuted);
        }
        return hasStockInPortfolio;
    }

    private LocalDateTime roundUpToMinutes(LocalDateTime time) {
        return LocalDateTime.of(time.getYear(), time.getMonth(), time.getDayOfMonth(), time.getHour(), time.getMinute(), 0, 0);
    }

    @Data
    @AllArgsConstructor
    private static class CandlesWithTime {
        List<CandleDto> candles;
        LocalDateTime time;
    }
}
