package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.CurrencyType;
import com.alexproj.tradingsystem.exceptions.IndicatorDataException;
import com.alexproj.tradingsystem.exceptions.StreamingException;
import com.alexproj.tradingsystem.notifications.ServiceNotification;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class Executor implements BotExecutor {
    public static final int DELAY_BETWEEN_BOT_WORKING_FAILED = 30000;
    private final TradingService tradingService;

    private final ServiceNotification telegramNotificationService;

    @Value("${bot.figis}")
    List<String> figis;

    @Value("${bot.currency}")
    String currency;

    private final BrokerApi brokerApi;

    @SuppressWarnings({"InfiniteLoopStatement", "BusyWait"})
    @Override
    @SneakyThrows
    public void startBot(boolean debugMode) {
        try {
            LocalDate now = LocalDate.now();
            String initMessage = debugMode ? "Бот запущен для отладки, не обращать внимания" : "Запускаем бота в реальном режиме...";

            telegramNotificationService.sendNotificationTech(initMessage);
            Thread.sleep(200);

            String brokerAccountId = brokerApi.getBrokerAccountId();

            while (true) {
                try {
                    if (!debugMode) {
                        BigDecimal currentBalance = brokerApi.getCurrencyBalance(brokerAccountId, CurrencyType.valueOf(currency));

                        String suffix = createStocksUsedMessage() + "Всем бодрости духа, например!";

                        String greetingMessage = "Сегодня " + now.getDayOfWeek()
                                + ", тащемта бот начинает свою работу на " + currentBalance.setScale(2, RoundingMode.HALF_UP)
                                + " " + getCurrencyMessageSuffix(CurrencyType.valueOf(currency)) + ". ";
                        telegramNotificationService.sendNotificationTech(greetingMessage + suffix);
                    }

                    tradingService.startSearchingStocks(brokerAccountId, figis);//filterFigisExcludeDividendNowStocks(figis));
                } catch (Exception e) {
                    String errorDetailMessage = "Причина неизвестна";
                    if (e instanceof StreamingException) {
                        errorDetailMessage = "Ошибка от стриминг сервиса цен тинькова";
                    }
                    if (e instanceof IndicatorDataException) {
                        errorDetailMessage = "Ошибка при расчете индикаторов";
                    }

                    e.printStackTrace();
                    //tradingService.stopSearchingStocks(filterFigisExcludeDividendNowStocks(figis));
                    telegramNotificationService.sendNotificationTech("Возникла ошибка, причина: "
                            + errorDetailMessage
                            + "Поиск акций приостановлен, бот перезапустится через 30 секунд");
                }
                Thread.sleep(DELAY_BETWEEN_BOT_WORKING_FAILED);
            }
        } catch (Exception e) {
            telegramNotificationService.sendNotificationTech("АЛЯРМА! Бот упал! необходимо его перезапустить!");
            log.error("An error has occurred while bot working");
            e.printStackTrace();
        }
    }

    private List<String> filterFigisExcludeDividendNowStocks(List<String> figis) {
        Map<String, Boolean> hasNoDividends = brokerApi.hasNoDividends(figis);
        return figis.stream()
                .filter(hasNoDividends::get)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @PreDestroy
    @SneakyThrows
    void destroy() {
        brokerApi.close();
    }

    private String getCurrencyMessageSuffix(CurrencyType type) {
        if (type == CurrencyType.RUB) {
            return "рублей";
        }
        if (type == CurrencyType.EUR) {
            return "евро";
        }
        if (type == CurrencyType.USD) {
            return "долларов";
        }
        if (type == CurrencyType.BTC) {
            return "биткоинов";
        }
        if (type == CurrencyType.USDT) {
            return "криптобаксов";
        }
        if (type == CurrencyType.BUSD) {
            return "криптобинансобаксов";
        }
        if (type == CurrencyType.BNB) {
            return "бинансовалют";
        }
        if (type == CurrencyType.ETH) {
            return "ефириумов";
        }
        return "";
    }

    private String createStocksUsedMessage() {
        StringBuilder figisNames = new StringBuilder();

        for (String figiName : brokerApi.getFigisNames(figis)) {
            figisNames.append(figiName).append("\n");
        }
        return "\nБот работает со следующими акциями: \n" + figisNames;
    }

}
