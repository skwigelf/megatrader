package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.CurrencyType;
import com.alexproj.tradingsystem.dto.PortfolioPositionDto;
import com.alexproj.tradingsystem.dto.SearchMarketInstrumentDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "bot", name = "roundLotsMode", havingValue = "rountToInt")
public class CalculationStrategyRoundToIntMode implements CalculationStrategy {
    private static final BigDecimal LOWER_COEFFICIENT = BigDecimal.valueOf(1.01);

    @Value("${bot.currency}")
    private String currency;

    @Value("${bot.marginal:false}")
    private boolean isMarge;

    private final BrokerApi brokerApi;

    @Override
    public BigDecimal calculateQuantity(SearchMarketInstrumentDto instrument, String brokerAccountId, BigDecimal price) {
        BigDecimal instrumentLots = instrument.getLot();

        BigDecimal highCoefficient = isMarge ? BigDecimal.valueOf(2) : BigDecimal.ONE;// instrument.getDlong() : BigDecimal.ONE;
        BigDecimal roubles = brokerApi.getCurrencyBalance(brokerAccountId, CurrencyType.valueOf(currency));
        int lots = instrumentLots == null
                ? 1
                : highCoefficient.multiply(roubles.divide(price.multiply(LOWER_COEFFICIENT).multiply(BigDecimal.valueOf(instrumentLots.toBigInteger().intValue())), 5, RoundingMode.HALF_DOWN)).intValue();
        return BigDecimal.valueOf(lots);
    }

    @Override
    public PortfolioPositionDto recalculateQuantity(PortfolioPositionDto portfolio, BigDecimal price) {
        return portfolio;
    }
}
