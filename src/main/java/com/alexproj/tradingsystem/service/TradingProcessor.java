package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.*;
import com.alexproj.tradingsystem.notifications.NotificationTechMessages;
import com.alexproj.tradingsystem.notifications.ServiceNotification;
import com.alexproj.tradingsystem.service.dto.*;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.alexproj.tradingsystem.service.TradingStatus.*;

@Slf4j
@Setter
@RequiredArgsConstructor
public class TradingProcessor {

    private final static int PREVIOUS_PRICES_NUMBER = 2;
    private final static int PREVIOUS_TRADES_NUMBER = 40;
    private String figi;

    private TradingStatus tradingStatus;

    private BigDecimal buyPrice;

    private AtomicBoolean disabled = new AtomicBoolean(false);

    private Queue<DecisionRequestPricesDto> prevPrices = new CircularFifoQueue<>(PREVIOUS_PRICES_NUMBER);

    private Queue<TradeDto> trades = new CircularFifoQueue<>(PREVIOUS_TRADES_NUMBER);

    private AtomicBoolean processing = new AtomicBoolean(false);

    private final CacheService cacheService;

    private final DecisionService decisionService;

    private final ServiceNotification telegramNotificationService;

    private final TradingService tradingService;

    private final Predicate<TradingStatus> filter;

    private final String buyDataType;

    private final String brokerType;

    private final TradeDbService dbService;

    private final boolean mock;

    public void handleOrderBook(OrderBookDto orderBookDto) {

    }

    public void handleTrade(TradeDto tradeDto) {
        if (tradeDto == null) {
            return;
        }
        trades.add(tradeDto);
        dbService.save(figi, tradeDto);
    }

    public void process(PriceDto priceDto, String brokerAccountId) {
        BigDecimal price = priceDto.getPrice();
        if (disabled.get() || !filter.test(tradingStatus)) {
            return;
        }
        if (processing.getAndSet(true)) {
            return;
        }

        if (price == null) {
            if (!mock) {
                log.info("Figi: " + figi + " bid price: " + priceDto.getBid() + " status: " + tradingStatus + (buyPrice == null ? "" : " buyprice: " + buyPrice));

            }
        } else {
            if (!mock) {
                log.info("Figi: " + figi + " price: " + price + " status: " + tradingStatus + (buyPrice == null ? "" : " buyprice: " + buyPrice));
            }

        }
        try {
            if (tradingStatus == CHECK_BUYING) {
                //tradingStatus = CHECK_SELLING;
                checkBuying(brokerAccountId, figi, priceDto);
            }
            if (tradingStatus == BUYING) {
                buying(brokerAccountId, figi, priceDto);
                return;
            }
            if (tradingStatus == TradingStatus.CHECK_SELLING) {
                checkSelling(brokerAccountId, figi, priceDto);
                return;
            }
            if (tradingStatus == TradingStatus.SELLING) {
                log.debug("stock has sold yet!");
            }
        } catch (Exception e) {
            telegramNotificationService.sendNotificationTech(NotificationTechMessages.errorWhileTrading(figi, tradingStatus, e));
            e.printStackTrace();

        } finally {
            processing.set(false);
        }
    }

    private void checkBuying(String brokerAccountId, String figi, PriceDto priceDto) throws InterruptedException, IOException {
        List<CandleDto> candles80Days = buyDataType.equals("candles") ? cacheService.getCandles(figi) : Collections.emptyList();

        BigDecimal price = priceDto.getPrice() == null ? priceDto.getBid() : priceDto.getPrice();

        candles80Days.get(candles80Days.size() - 1).setClose(price);

        List<DecisionRequestPricesDto> lastPrices = buyDataType.equals("lastPrices") ? addPriceAndGetPreviousPrices(price) : Collections.emptyList();

        BuyDecisionDto decision = decisionService.buyDecision(figi, lastPrices, new ArrayList<>(trades), candles80Days);

        if (decision.isDecision()) {
            //tradingStatus = BUYING;
            buying(brokerAccountId, figi, priceDto);
            return;
        }

        if (decision.getData() != null) {
            decision.getData().setMarketInstrument(cacheService.getMarketInstrument(figi));
            if (!mock) {
                log.info("Figi: " + figi +
                        " Bollinger down: " + decision.getData().getBollingerDown() +
                        " RSI: " + decision.getData().getRsi() +
                        " price:" + price);
            }
        }

        if (decision.isDecision()) {
            //telegramNotificationService.sendNotificationTech(NotificationTechMessages.buyDecisionMessage(decision.getData().getMarketInstrument().getName(), decision.getData()));
        }
    }

    private void buying(String brokerAccountId, String figi, PriceDto priceDto) throws InterruptedException {
        SearchMarketInstrumentDto marketInstrument = cacheService.getMarketInstrument(figi);

        BigDecimal buyPrice = priceDto.getPrice() == null ? priceDto.getAsk() : priceDto.getPrice();

        OrderDto activeOrder = tradingService.getActiveOrder(brokerAccountId, figi);
        boolean hasStockInPortfolio = tradingService.hasProductInPortfolio(brokerAccountId, figi);

        if (activeOrder == null && !hasStockInPortfolio) {
            BuyResultDto buyResultDto = tradingService.buy(marketInstrument, buyPrice, brokerAccountId);
            if (buyResultDto.isDecision()) {
                tradingStatus = CHECK_SELLING;
                this.buyPrice = buyResultDto.getBuyPrice();
            } else {
                tradingStatus = CHECK_BUYING;
            }
        }
    }

    private void checkSelling(String brokerAccountId, String figi, PriceDto priceDto) throws
            InterruptedException, IOException {


        if (!mock && brokerType.equals("tinkoff") && !cacheService.hasStockInPortfolio(figi)) {
            tradingStatus = CHECK_BUYING;
            return;
        }
        BigDecimal price = priceDto.getPrice() == null ? priceDto.getBid() : priceDto.getPrice();

        SellDecisionDto decision = decisionService.sellDecision(figi, buyPrice, addPriceAndGetPreviousPrices(price));//addPriceAndGetPreviousPrices(price));

        if (decision.isDecision()) {
            selling(brokerAccountId, figi, priceDto);
        }
    }

    private void selling(String brokerAccountId, String figi, PriceDto priceDto) throws InterruptedException {

        BigDecimal price = priceDto.getPrice() == null ? priceDto.getBid() : priceDto.getPrice();
        boolean sellDecision = tradingService.sell(figi, brokerAccountId, buyPrice, price);

        if (sellDecision) {
            prevPrices.clear();
        }
        tradingStatus = sellDecision ? CHECK_BUYING : CHECK_SELLING;
    }

    private List<DecisionRequestPricesDto> addPriceAndGetPreviousPrices(BigDecimal price) {
        prevPrices.offer(new DecisionRequestPricesDto(price, LocalDateTime.now()));

        return prevPrices.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }
}
