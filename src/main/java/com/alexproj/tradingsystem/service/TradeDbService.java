package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.model.Trade;
import com.alexproj.tradingsystem.service.dto.TradeDto;

public interface TradeDbService {
    Trade save(String figi, TradeDto tradeDto);
}
