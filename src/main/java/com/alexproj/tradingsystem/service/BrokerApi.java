package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.*;
import com.alexproj.tradingsystem.dto.LimitOrderRequestDto;
import com.alexproj.tradingsystem.dto.OperationTypeWithCommission;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface BrokerApi {

    BigDecimal getCurrencyBalance(String brokerAccountId, CurrencyType currencyType);

    List<String> getFigisNames(List<String> figis);


    /**
     * Поиск позиции в портфеле
     * @param figi
     * @param instrument Необязательный параметр. В случае отсутствия количество лотов может быть рассчитано некорректно
     * @param brokerAccountId
     * @return
     */
    PortfolioPositionDto getPortfolioPosition(String figi, SearchMarketInstrumentDto instrument, String brokerAccountId);

    List<OrderDto> getActiveOrders(String brokerAccountId, String figi);

    OrderDto getActiveOrder(String brokerAccountId, String figi, String orderId);

    SearchMarketInstrumentDto searchByFigi(String figi);

    OperationDto getLastPositionLastDay(String figi, String brokerAccountId, OperationTypeWithCommission type);

    void cancelOrder(String brokerAccountId, String figi, String id);

    PlacedLimitOrderDto placeLimitOrder(String brokerAccountId, String figi, LimitOrderRequestDto limitOrderRequest);

    void close() throws IOException;

    String getBrokerAccountId();

    Map<String, Boolean> hasNoDividends(List<String> figis);
}
