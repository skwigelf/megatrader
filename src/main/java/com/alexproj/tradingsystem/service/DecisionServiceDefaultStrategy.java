package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.BuyDecisionDto;
import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.dto.SellDecisionDto;
import com.alexproj.tradingsystem.notifications.ServiceNotification;
import com.alexproj.tradingsystem.service.dto.DecisionRequestPricesDto;
import com.alexproj.tradingsystem.service.dto.IndicatorDto;
import com.alexproj.tradingsystem.service.dto.TradeDto;
import com.alexproj.tradingsystem.service.tinkoff.decisionrules.DefaultBuyRule;
import com.alexproj.tradingsystem.service.tinkoff.decisionrules.DefaultTradesBuyRule;
import com.alexproj.tradingsystem.service.tinkoff.decisionrules.RualBuyRule;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import static java.math.BigDecimal.valueOf;

@Slf4j
@Service
@ConditionalOnProperty(prefix = "bot", name = "strategy", havingValue = "default", matchIfMissing = true)
@RequiredArgsConstructor
public class DecisionServiceDefaultStrategy implements DecisionService {

    private static final BigDecimal BOLLINGER_DOWN_LIMIT = valueOf(0.006);
    private static final BigDecimal RSI_LIMIT = BigDecimal.valueOf(40);
    public static final BigDecimal MIN_SELL_PRICE = BigDecimal.valueOf(0.0015);

    private final IndicatorService indicatorService;
    private final ServiceNotification notificationService;
    private final CacheService cacheService;

    private final Map<String, StopLossDto> stopLosses = new ConcurrentHashMap<>();
    private final Map<String, Function<IndicatorDto, Boolean>> customMapRules = new ConcurrentHashMap<>();

    private final List<Function<IndicatorDto, Boolean>> defaultRules = new ArrayList<>();

    private static final BigDecimal FIRST_STOP = valueOf(-0.01);
    private static final BigDecimal SECOND_STOP = valueOf(-0.02);
    private static final BigDecimal THIRD_STOP = valueOf(-0.03);
    private static final BigDecimal FOURTH_STOP = valueOf(-0.05);
    private static final BigDecimal FIFTH_STOP = valueOf(-0.1);

    private static final String FIRST_STOP_MESSAGE = "Акция упала на процент!";
    private static final String SECOND_STOP_MESSAGE = "АКЦИЯ УПАЛА НА 2 ПРОЦЕНТА!РЕКОМЕНДУЕТСЯ СЛИТЬ АКЦИЮ И ПЕРЕЗАПУСТИТЬ БОТА";
    private static final String THIRD_STOP_MESSAGE = "АКЦИЯ УПАЛА НА 3 ПРОЦЕНТА!";
    private static final String FOURTH_STOP_MESSAGE = "АКЦИЯ УПАЛА НА 5 ПРОЦЕНТОВ!!!";
    private static final String FIFTH_STOP_MESSAGE = "АКЦИЯ УПАЛА НА 10 ПРОЦЕНТОВ";

    @PostConstruct
    public void setUp() {
        customMapRules.put("BBG008F2T3T2", new RualBuyRule());
        defaultRules.add(new DefaultBuyRule());
        defaultRules.add(new DefaultTradesBuyRule());
    }

    @SuppressWarnings("StatementWithEmptyBody")
    public SellDecisionDto sellDecision(String figi, @NonNull BigDecimal buyPrice, List<DecisionRequestPricesDto> prices) {
        StopLossDto stopLossDto = stopLosses.get(figi);

        if (stopLossDto == null) {
            stopLosses.put(figi, new StopLossDto());
        }
        stopLossDto = stopLosses.get(figi);

        BigDecimal lastPrice = prices.get(prices.size() - 1).getPrice();

        BigDecimal percentageDifference = lastPrice.subtract(buyPrice).divide(buyPrice, 5, RoundingMode.HALF_UP);
        if (percentageDifference.compareTo(stopLossDto.getStopLossHard()) < 0) {
            return new SellDecisionDto(true, lastPrice);
        }

        if (percentageDifference.compareTo(FIRST_STOP) < 0 && !stopLossDto.isReachedFirstStop()) {
            notificationService.sendNotificationTech(FIRST_STOP_MESSAGE);
            stopLossDto.setReachedFirstStop(true);
        }
        if (percentageDifference.compareTo(SECOND_STOP) < 0 && !stopLossDto.isReachedSecondStop()) {
            notificationService.sendNotificationTech(SECOND_STOP_MESSAGE);
            stopLossDto.setReachedSecondStop(true);
        }
        if (percentageDifference.compareTo(THIRD_STOP) < 0 && !stopLossDto.isReachedThirdStop()) {
            notificationService.sendNotificationTech(THIRD_STOP_MESSAGE);
            stopLossDto.setReachedThirdStop(true);
        }
        if (percentageDifference.compareTo(FOURTH_STOP) < 0 && !stopLossDto.isReachedFourthStop()) {
            notificationService.sendNotificationTech(FOURTH_STOP_MESSAGE);
            stopLossDto.setReachedFourthStop(true);
        }
        if (percentageDifference.compareTo(FIFTH_STOP) < 0 && !stopLossDto.isReachedFifthStop()) {
            notificationService.sendNotificationTech(FIFTH_STOP_MESSAGE);
            stopLossDto.setReachedFifthStop(true);
        }

        if (percentageDifference.compareTo(stopLossDto.getTakeProfit()) > 0) {
            stopLossDto.setStopLoss(stopLossDto.getTakeProfit().subtract(stopLossDto.getSlDownStep()));
            stopLossDto.setTakeProfit(stopLossDto.getTakeProfit().add(stopLossDto.getTpStep()));
        }
        if (ifAllPricesReachedOutStopLoss(buyPrice, prices, stopLossDto)) {//if (percentageDifference.compareTo(stopLossDto.getStopLoss()) < 0) {
            stopLossDto.setTakeProfit(valueOf(0.003));
            stopLossDto.setStopLoss(valueOf(-9999));
            stopLossDto.setTpStep(valueOf(0.001));

            stopLossDto.resetStops();

            BigDecimal minPriceIncrement = cacheService.getMarketInstrument(figi).getMinPriceIncrement();
            if (percentageDifference.compareTo(MIN_SELL_PRICE) < 0) {
                lastPrice = calculatePriceByMinSellPoint(minPriceIncrement, buyPrice);
            }
            return new SellDecisionDto(true, lastPrice);
        }

        return new SellDecisionDto(false, lastPrice);
    }

    public BuyDecisionDto buyDecision(String figi, List<DecisionRequestPricesDto> newPrices, List<TradeDto> trades, List<CandleDto> candles80Days) {
        BigDecimal bollingerDown = indicatorService.bollingerLow(candles80Days);

        CandleDto lastCandle = candles80Days.get(candles80Days.size() - 1);
        BigDecimal price = lastCandle.getClose();

        IndicatorDto data = new IndicatorDto(null, bollingerDown, null, price, trades);

        Function<IndicatorDto, Boolean> decisionRule = getCustomBuyRule(figi);
        boolean buyDefaultDecision = defaultRules.stream().anyMatch(rule -> rule.apply(data));
        boolean decision = decisionRule != null && decisionRule.apply(data);

        return new BuyDecisionDto(data, decision || buyDefaultDecision);
    }

    private boolean ifAllPricesReachedOutStopLoss(BigDecimal buyPrice, List<DecisionRequestPricesDto> prices, StopLossDto stopLossDto) {
        return prices.stream().map(DecisionRequestPricesDto::getPrice).allMatch(price -> {
            BigDecimal percentageDifference = price.subtract(buyPrice).divide(buyPrice, 5, RoundingMode.HALF_UP);
            return percentageDifference.compareTo(stopLossDto.getStopLoss()) < 0;
        });
    }

    private Function<IndicatorDto, Boolean> getCustomBuyRule(String figi) {
        return customMapRules.get(figi);// == null ? new DefaultBuyRule() : decisionRule;
    }

    private BigDecimal calculatePriceByMinSellPoint(BigDecimal minPriceIncrement, BigDecimal buyPrice) {
        int scale = minPriceIncrement.scale();

        BigDecimal price = buyPrice.multiply(MIN_SELL_PRICE).add(buyPrice).setScale(scale, RoundingMode.HALF_DOWN);

        //Расчет цены с учетом шага (шаг берется из инфы по акции)
        String priceString = price.toString();
        String minPriceStr = minPriceIncrement.toString();
        int priceInt = Integer.parseInt(priceString.substring(priceString.length() - 1));
        int minPriceInt = Integer.parseInt(minPriceStr.substring(minPriceStr.length() - 1));

        if (minPriceInt > 1) {
            int diffInt = priceInt % minPriceInt;

            BigDecimal diff = BigDecimal.valueOf(diffInt, minPriceIncrement.scale());
            price = price.subtract(diff);
        }

        return price;
    }

    @Data
    private static class StopLossDto {
        private BigDecimal takeProfit = valueOf(0.003);
        private BigDecimal stopLoss = valueOf(-9999);
        private BigDecimal tpStep = valueOf(0.001);
        private BigDecimal slDownStep = valueOf(0.001);
        private BigDecimal stopLossHard = valueOf(-0.19);

        private boolean isReachedFirstStop = false;
        private boolean isReachedSecondStop = false;
        private boolean isReachedThirdStop = false;
        private boolean isReachedFourthStop = false;
        private boolean isReachedFifthStop = false;

        public void resetStops() {
            isReachedFirstStop = false;
            isReachedSecondStop = false;
            isReachedThirdStop = false;
            isReachedFourthStop = false;
            isReachedFifthStop = false;
        }
    }

}
