package com.alexproj.tradingsystem.service;

import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.dto.SearchMarketInstrumentDto;

import java.util.List;

public interface CacheService {
    List<CandleDto> getCandles(String figi);

    SearchMarketInstrumentDto getMarketInstrument(String figi);

    boolean hasStockInPortfolio(String figi);
}
