package com.alexproj.tradingsystem.model;

import com.alexproj.tradingsystem.service.dto.TradeDirection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "trades")
public class Trade {

    @Id
    private UUID id = UUID.randomUUID();

    private String figi;
    private long quantity;
    @Enumerated(EnumType.STRING)
    private TradeDirection direction;
    private BigDecimal price;
    private OffsetDateTime time;
}
