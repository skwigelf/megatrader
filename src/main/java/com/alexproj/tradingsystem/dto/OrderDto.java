package com.alexproj.tradingsystem.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class OrderDto {
    private String figi;
}
