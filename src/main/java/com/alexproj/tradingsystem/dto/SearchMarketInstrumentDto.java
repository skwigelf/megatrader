package com.alexproj.tradingsystem.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
public class SearchMarketInstrumentDto {

    private String figi;
    private String name;
    private BigDecimal lot;
    private BigDecimal minNotional;
    private BigDecimal minPriceIncrement;

    private BigDecimal dlong;
}
