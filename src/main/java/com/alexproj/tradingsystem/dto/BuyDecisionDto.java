package com.alexproj.tradingsystem.dto;

import com.alexproj.tradingsystem.service.dto.IndicatorDto;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BuyDecisionDto {
    private IndicatorDto data;
    private boolean decision;
}
