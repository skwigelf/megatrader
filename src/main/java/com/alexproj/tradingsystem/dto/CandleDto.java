package com.alexproj.tradingsystem.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.OffsetDateTime;

@Data
@AllArgsConstructor

@Builder
public class CandleDto {
    private BigDecimal open;
    private BigDecimal close;
    private BigDecimal high;
    private BigDecimal low;
    private Long volume;
    private OffsetDateTime time;
    private String figi;
    private Duration interval;
}
