package com.alexproj.tradingsystem.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Builder
@Data
public class OperationDto {
    private String figi;
    private BigDecimal price;
    private OffsetDateTime date;
    private OperationTypeWithCommission operationType;
}
