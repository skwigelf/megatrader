package com.alexproj.tradingsystem.dto;

public enum OperationTypeWithCommission {
    BUY,
    BUYCARD,
    SELL,
    BROKERCOMMISSION,
    EXCHANGECOMMISSION,
    SERVICECOMMISSION,
    MARGINCOMMISSION,
    OTHERCOMMISSION,
    PAYIN,
    PAYOUT,
    TAX,
    TAXLUCRE,
    TAXDIVIDEND,
    TAXCOUPON,
    TAXBACK,
    REPAYMENT,
    PARTREPAYMENT,
    COUPON,
    DIVIDEND,
    SECURITYIN,
    SECURITYOUT
}
