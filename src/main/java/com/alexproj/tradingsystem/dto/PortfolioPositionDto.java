package com.alexproj.tradingsystem.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
public class PortfolioPositionDto {

    private String figi;
    private BigDecimal lots;
    private LocalDateTime time;
}
