package com.alexproj.tradingsystem.dto.streaming;

public enum StreamingSubscriptionAction {
    SUBSCRIBE,
    UNSUBSCRIBE
}
