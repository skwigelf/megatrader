package com.alexproj.tradingsystem.dto;

import com.fasterxml.jackson.annotation.JsonValue;

public enum MarketInstrumentType {
    STOCK("Stock"),
    CURRENCY("Currency"),
    BOND("Bond"),
    ETF("Etf");

    private final String value;

    MarketInstrumentType(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
