package com.alexproj.tradingsystem.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.tinkoff.invest.openapi.model.rest.Currency;

import java.math.BigDecimal;

@AllArgsConstructor
@Builder
@Data
public class MarketInstrumentDto {
    private String figi;
    private String ticker;
    private String isin;
    private BigDecimal minPriceIncrement;
    private Integer lot;
    private Integer minQuantity;
    private Currency currency;
    private String name;
    private MarketInstrumentType type;
}
