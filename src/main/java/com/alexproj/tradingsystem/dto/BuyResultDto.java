package com.alexproj.tradingsystem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class BuyResultDto {
    private boolean decision;
    private BigDecimal buyPrice;
}
