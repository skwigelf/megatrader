package com.alexproj.tradingsystem.dto;

public enum OperationType {
    BUY,
    SELL
}
