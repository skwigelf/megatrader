package com.alexproj.tradingsystem.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
public class LimitOrderRequestDto {
    private BigDecimal lots;
    //ИСПОЛЬЗУЕТСЯ ДЛЯ КРИПТЫ И ТОЛЬКО. ВМЕСТО ЛОТОВ
    private BigDecimal quantity;
    private OperationType operation;
    private BigDecimal price;
}
