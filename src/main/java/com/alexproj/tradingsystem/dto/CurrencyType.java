package com.alexproj.tradingsystem.dto;

public enum CurrencyType {
    RUB,
    USD,
    EUR,
    USDT,
    BNB,
    BUSD,
    BTC,
    ETH
}
