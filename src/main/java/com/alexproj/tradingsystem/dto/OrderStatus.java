package com.alexproj.tradingsystem.dto;

public enum OrderStatus {
    NEW,
    PARTIALLYFILL,
    FILL,
    CANCELLED,
    REPLACED,
    PENDINGCANCEL,
    REJECTED,
    PENDINGREPLACE,
    PENDINGNEW
}
