package com.alexproj.tradingsystem.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Builder
@Data
public class PlacedLimitOrderDto {

    private OrderStatus status;
    private String orderId;
}
