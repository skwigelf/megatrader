package com.alexproj.tradingsystem.web;

import com.alexproj.tradingsystem.service.BotExecutor;
import com.alexproj.tradingsystem.service.testMock.ExecutorMock;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

@RestController
@RequestMapping("/api/rest/bot")
@RequiredArgsConstructor
public class BotController {

    private final BotExecutor botExecutor;
    private final ExecutorMock executorMock;

    @Value("${bot.mock:false}")
    boolean isTestMode;

    private final AtomicBoolean isBotAlreadyStarted = new AtomicBoolean(false);

    private final ExecutorService pool = Executors.newFixedThreadPool(1);

    private static final BigDecimal COMMISION = BigDecimal.valueOf(0.001);

    @PostMapping
    public void startBot(@RequestParam(required = false) boolean debugMode) {

        if (isBotAlreadyStarted.get()) {
            throw new RuntimeException("Bot has already started");
        }
        CompletableFuture.runAsync(() -> botExecutor.startBot(debugMode), pool);
        isBotAlreadyStarted.set(true);
    }

    @PostMapping("test")
    public void startTesting(){
        if (!isTestMode) {
            throw new RuntimeException("");
        }

        executorMock.start();
    }

    @PostMapping("crypto")
    public void cryptoTest() throws InterruptedException {
        BigDecimal price = BigDecimal.valueOf(11.02270000);

        BigDecimal lots = BigDecimal.valueOf(6.10);
        lots = lots.setScale(2);

        lots = lots.multiply(price)
                .multiply(BigDecimal.ONE.subtract(COMMISION)).divide(price, lots.scale(), RoundingMode.HALF_DOWN);

//
//       // BigDecimal usdt = brokerApi.getCurrencyBalance("", CurrencyType.USDT);
//        SearchMarketInstrumentDto result = brokerApi.searchByFigi("BTCUSDT");
//
//        //streamingInfoService.subscribePrices(asList("btcusdt"), "", null);
//
//        BigDecimal resAlT = result.getMinPriceIncrement();
    }
}
