package com.alexproj.tradingsystem.web;

import com.alexproj.tradingsystem.service.ReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rest/history")
@RequiredArgsConstructor
public class HistoryInfoController {

    private final ReportService reportService;

    @GetMapping
    public void create() {
        reportService.createReport();
    }
}
