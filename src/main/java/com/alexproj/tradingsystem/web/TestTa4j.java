package com.alexproj.tradingsystem.web;

import com.alexproj.tradingsystem.dto.CandleDto;
import com.alexproj.tradingsystem.repo.Ta4jIndicatorRepository;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TestTa4j {
    public static void main(String[] args) {
        Ta4jIndicatorRepository ta4jIndicatorRepository = new Ta4jIndicatorRepository();

        List<CandleDto> candleDtoList = new ArrayList<>();
        double[] candles = {10.0, 2.0, 5.0, 8.0, 4.0, 9.0, 7.0, 1.0, 5.0, 6.5, 10.0, 2.0, 5.0, 8.0, 4.0, 9.0, 7.0, 1.0, 5.0, 6.5
                , 10.0, 2.0, 5.0, 8.0, 4.0, 9.0, 7.0, 1.0, 5.0, 6.5};
        BigDecimal big = ta4jIndicatorRepository.bollingerUp(createCandle(candles), 20);

        System.out.println(big);
    }

    private static List<CandleDto> createCandle(double[] candle) {
        return Arrays.stream(candle).mapToObj(x -> {
            return CandleDto.builder()
                    .close(BigDecimal.valueOf(x))
                    .open(BigDecimal.valueOf(x))
                    .high(BigDecimal.valueOf(x))
                    .low(BigDecimal.valueOf(x))
                    .volume(10600L)
                    .figi("123")
                    .interval(Duration.ZERO)
                    .time(OffsetDateTime.now())
                    .build();
        }).collect(Collectors.toList());
    }
}
