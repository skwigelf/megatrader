package com.alexproj.tradingsystem.config;

import com.alexproj.tradingsystem.interceptors.AuthInterceptor;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import ru.tinkoff.invest.openapi.OpenApi;
import ru.tinkoff.invest.openapi.okhttp.OkHttpOpenApi;
import ru.tinkoff.piapi.contract.v1.*;

import java.time.Clock;

import static io.grpc.Metadata.ASCII_STRING_MARSHALLER;

@SuppressWarnings("deprecation")
@Configuration
@EnableAsync
@EnableScheduling
public class ApiConfig extends WebMvcConfigurerAdapter {

    @Value("${bot.token}")
    private String token;

    @Bean("openApi")
    public OpenApi openApi() {

        boolean sandboxMode = false;
        return new OkHttpOpenApi(token, sandboxMode);
    }

    @Bean("managedChannel")
    public ManagedChannel managedChannel() {
        return ManagedChannelBuilder.forAddress("invest-public-api.tinkoff.ru", 443).build();
    }

    @Bean
    @DependsOn({"managedChannel"})
    public MarketDataStreamServiceGrpc.MarketDataStreamServiceStub marketDataStreamServiceStub(
            ManagedChannel managedChannel) {
        MarketDataStreamServiceGrpc.MarketDataStreamServiceStub stub =
                MarketDataStreamServiceGrpc.newStub(managedChannel);

        Metadata.Key<String> key = Metadata.Key.of("Authorization", ASCII_STRING_MARSHALLER);
        Metadata metadata = new Metadata();
        metadata.put(key, "Bearer " + token);
        return MetadataUtils.attachHeaders(stub, metadata);
    }

    @Bean
    @DependsOn({"managedChannel"})
    public MarketDataServiceGrpc.MarketDataServiceBlockingStub marketDataServiceBlockingStub(
            ManagedChannel managedChannel) {
        MarketDataServiceGrpc.MarketDataServiceBlockingStub stub =
                MarketDataServiceGrpc.newBlockingStub(managedChannel);

        Metadata.Key<String> key = Metadata.Key.of("Authorization", ASCII_STRING_MARSHALLER);
        Metadata metadata = new Metadata();
        metadata.put(key, "Bearer " + token);
        return MetadataUtils.attachHeaders(stub, metadata);
    }

    @Bean
    @DependsOn({"managedChannel"})
    public InstrumentsServiceGrpc.InstrumentsServiceBlockingStub instrumentsServiceBlockingStub(
            ManagedChannel managedChannel) {
        InstrumentsServiceGrpc.InstrumentsServiceBlockingStub stub =
                InstrumentsServiceGrpc.newBlockingStub(managedChannel);

        Metadata.Key<String> key = Metadata.Key.of("Authorization", ASCII_STRING_MARSHALLER);
        Metadata metadata = new Metadata();
        metadata.put(key, "Bearer " + token);
        return MetadataUtils.attachHeaders(stub, metadata);
    }

    @Bean
    @DependsOn({"managedChannel"})
    public OperationsServiceGrpc.OperationsServiceBlockingStub operationsServiceBlockingStub(
            ManagedChannel managedChannel) {
        OperationsServiceGrpc.OperationsServiceBlockingStub stub =
                OperationsServiceGrpc.newBlockingStub(managedChannel);

        Metadata.Key<String> key = Metadata.Key.of("Authorization", ASCII_STRING_MARSHALLER);
        Metadata metadata = new Metadata();
        metadata.put(key, "Bearer " + token);
        return MetadataUtils.attachHeaders(stub, metadata);
    }

    @Bean
    @DependsOn({"managedChannel"})
    public OrdersServiceGrpc.OrdersServiceBlockingStub ordersServiceBlockingStub(
            ManagedChannel managedChannel) {
        OrdersServiceGrpc.OrdersServiceBlockingStub stub =
                OrdersServiceGrpc.newBlockingStub(managedChannel);

        Metadata.Key<String> key = Metadata.Key.of("Authorization", ASCII_STRING_MARSHALLER);
        Metadata metadata = new Metadata();
        metadata.put(key, "Bearer " + token);
        return MetadataUtils.attachHeaders(stub, metadata);
    }

    @Bean
    @DependsOn({"managedChannel"})
    public UsersServiceGrpc.UsersServiceBlockingStub usersServiceBlockingStub(
            ManagedChannel managedChannel) {
        UsersServiceGrpc.UsersServiceBlockingStub stub =
                UsersServiceGrpc.newBlockingStub(managedChannel);

        Metadata.Key<String> key = Metadata.Key.of("Authorization", ASCII_STRING_MARSHALLER);
        Metadata metadata = new Metadata();
        metadata.put(key, "Bearer " + token);
        return MetadataUtils.attachHeaders(stub, metadata);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthInterceptor())
                .addPathPatterns("/**");
    }

    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }

}
