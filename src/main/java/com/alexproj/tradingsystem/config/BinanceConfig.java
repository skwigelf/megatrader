package com.alexproj.tradingsystem.config;

import com.binance.connector.client.SpotClient;
import com.binance.connector.client.WebsocketClient;
import com.binance.connector.client.impl.SpotClientImpl;
import com.binance.connector.client.impl.WebsocketClientImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@EnableAsync
public class BinanceConfig {
    @Bean("SpotClient")
    public SpotClient openApi() {
        return new SpotClientImpl(BinanceConfigPrivate.API_KEY, BinanceConfigPrivate.SECRETKEY);
    }

    @Bean("WebsocketClient")
    public WebsocketClient socketClient() {
        return new WebsocketClientImpl();
    }
}
