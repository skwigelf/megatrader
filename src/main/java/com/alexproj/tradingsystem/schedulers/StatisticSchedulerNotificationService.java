package com.alexproj.tradingsystem.schedulers;

import com.alexproj.tradingsystem.notifications.ServiceNotification;
import com.alexproj.tradingsystem.service.BrokerApi;
import com.alexproj.tradingsystem.service.StatService;
import com.alexproj.tradingsystem.service.dto.FullDealInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class StatisticSchedulerNotificationService {

    private final ServiceNotification telegramNotificationService;

    private final BrokerApi brokerApi;

    private final StatService statService;

    private final static BigDecimal depositBalance = BigDecimal.valueOf(150000);

    @Scheduled(cron = "0 0 22 * * ?", zone = "Europe/Moscow")
    public void dayResultNotify() {
        String brokerAccountId = brokerApi.getBrokerAccountId();
        OffsetDateTime now = OffsetDateTime.now();
        Map<String, List<FullDealInfo>> dayDeals = statService.getDayDeals(brokerAccountId, now);

        BigDecimal balance = BigDecimal.ZERO;
        int dayStocksNumber = 0;
        int profitDealsNumber = 0;
        int lossDealsNumber = 0;

        for (Map.Entry<String, List<FullDealInfo>> entry : dayDeals.entrySet()) {
            for (FullDealInfo fullDealInfo : entry.getValue()) {
                BigDecimal diff = fullDealInfo.getBuyPrice()
                        .add(fullDealInfo.getCommission())
                        .add(fullDealInfo.getSellPrice());

                balance = balance.add(diff);

                if (diff.compareTo(BigDecimal.ZERO) >= 0) {
                    profitDealsNumber++;
                } else {
                    lossDealsNumber++;
                }
                dayStocksNumber++;
            }
        }

        String message = "";
        if (balance.compareTo(BigDecimal.ZERO) > 0) {
            message += "Тащемта день успешен: " + "\n";
        } else if (balance.compareTo(BigDecimal.ZERO) == 0) {
            message += "Сегодня прибыль отсутствует: " + "\n";
        } else {
            message += "День неуспешен: " + "\n";
        }
        message += "Прибыль за день: \n В абсолютном значении : " + balance + "\n"
                + "Относительно изначального депозита: "
                + balance.divide(depositBalance, 5, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100)).setScale(3, RoundingMode.HALF_UP)
                + "%\n"
                + "Количество всех сделок: " + dayStocksNumber + "\n"
                + "Количество успешных сделок: " + profitDealsNumber + "\n"
                + "Количество херовых сделок: " + lossDealsNumber;
        telegramNotificationService.sendNotification(message);
    }
}
