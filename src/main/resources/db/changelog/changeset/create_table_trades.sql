CREATE TABLE TRADES (
    id uuid primary key,
    figi varchar(15) not null,
    quantity decimal not null,
    direction varchar(28),
    price decimal(12,6),
    time timestamp not null
);